
================================================================================
= README.TXT                                                                   =
================================================================================

Installation instructions:
--------------------------

Medbook HELPDESK installation should not take more than 15 minutes to complete.
Details are provided in this article:

https://support.faveohelpdesk.com/knowledgebase

Upgrade instructions:
---------------------

If you are upgrading your Medbook HELPDESK to the latest stable release, please
follow the instructions provided in this article:

https://support.faveohelpdesk.com/knowledgebase

================================================================================

Thank you for choosing Medbook HELPDESK! We hope that you will enjoy using it as
much as we enjoy developing it!
