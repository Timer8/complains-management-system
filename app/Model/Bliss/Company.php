<?php

namespace App\Model\Bliss;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = "companies";

    protected $fillable = ["name","date_added","date_deleted","date_updated"];
}
