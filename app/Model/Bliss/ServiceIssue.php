<?php

namespace App\Model\Bliss;

use Illuminate\Database\Eloquent\Model;

class ServiceIssue extends Model
{
    protected $table = "service_issues";

    protected $fillable = ["name", "service_id", "deleted_at", "created_at", "updated_at"];

    public function escalations()
    {
        return $this->hasMany(UserEscalation::class, 'service_issue_id', 'id');
    }
}
