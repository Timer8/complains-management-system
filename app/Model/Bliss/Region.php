<?php

namespace App\Model\Bliss;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $table = "regions";

    protected $fillable = ["name","deleted_at","created_at","updated_at"];
}
