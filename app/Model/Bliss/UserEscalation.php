<?php

namespace App\Model\Bliss;

use Illuminate\Database\Eloquent\Model;

class UserEscalation extends Model
{
    protected $table = "user_escalations";

    protected $fillable = ["user","region_id","company_id","service_issue_id","escalation_level","deleted_at","created_at","updated_at","minutes"];

    public function region(){
        return $this->hasOne(Region::class,'id','region_id');
        return $this->hasOne(Company::class,'id','company_id');
    }
}
