<?php

namespace App\Model\Bliss;

use Illuminate\Database\Eloquent\Model;

class ReportChannel extends Model
{
    protected $table = "report_channels";

    protected $fillable = ["name","deleted_at","created_at","updated_at"];
}
