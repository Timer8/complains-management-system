<?php

namespace App\Model\Bliss;

use Illuminate\Database\Eloquent\Model;

class Facility extends Model
{
    protected $table = "facilities";

    protected $fillable = ["name","company_id","region_id","business_filter","deleted_at","created_at","updated_at"];
}
