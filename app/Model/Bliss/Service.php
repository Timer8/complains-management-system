<?php

namespace App\Model\Bliss;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = "services";

    protected $fillable = ["name","deleted_at","created_at","updated_at"];

    public function serviceissues(){
       return $this->hasMany(ServiceIssue::class,'service_id','id');
    }
}
