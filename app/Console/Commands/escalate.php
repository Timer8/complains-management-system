<?php

namespace App\Console\Commands;

use App\Http\Controllers\Agent\helpdesk\TicketController;
use App\Model\Bliss\Facility;
use App\Model\Bliss\UserEscalation;
use App\Model\helpdesk\Settings\Ticket;
use App\Model\helpdesk\Ticket\Tickets;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Console\Command;
use Zend\Validator\Date;

class escalate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'escalate:tickets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ticket escalation tasks';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->escalation();

    }

    /**
     * @param $level integer
     */
    private function escalation()
    {
        $PhpMailController = new \App\Http\Controllers\Common\PhpMailController();
        $NotificationController = new \App\Http\Controllers\Common\NotificationController();
        $ticketController = new TicketController($PhpMailController, $NotificationController);
        $time =  (Carbon::now())->subMinutes(10);
        $tickets = Tickets::where('updated_at', '<', $time)
            ->where('escalation_level', '<',4)
            ->get();
        foreach ($tickets as $ticket) {
            $facility = Facility::find($ticket->facility);
            \Log::info("facility: " . $facility->name);
            if ($facility) {
                \Log::info("facility found: " . $facility->name);
                \Log::info("searching escalation");
                $userEscalation = UserEscalation::where('region_id', $facility->region_id)
                    ->where('service_issue_id', $ticket->service_issue)
                    ->where('escalation_level', $ticket->escalation_level+1)
                    ->first();
                \Log::info("escalation:");
                \Log::info($userEscalation);
                if ($userEscalation) {
                    $updatedAt = Carbon::parse($ticket->updated_at);
                    if ($userEscalation->minutes > 0)
                        if (Carbon::now()->diffInMinutes($updatedAt) > $userEscalation->minutes) {
                            \Log::info("assigning ticket: " . $ticket->id);
                            $user = User::where('email', $userEscalation->user)->first();
                            try {
                                $assignto = $ticketController->assign($ticket->id, "user_" . $user->id,  $ticket->escalation_level+1);
                            } catch (Exception $e) {
                                \Log::info("Error " . $e->getMessage());
                            }
                        }
                }
            }
        }
    }
}
