<?php

namespace App\Http\Controllers\Agent\helpdesk;

// controllers
use App\Http\Controllers\Controller;
use App\Model\helpdesk\Manage\Help_topic;
// request
use App\Model\helpdesk\Ticket\Ticket_source;
// Model
use Illuminate\Http\Request;
// classes
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PDF;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

/**
 * ReportController
 * This controlleris used to fetch reports in the agent panel.
 *
 * @author      Ladybird <info@ladybirdweb.com>
 */
class ReportController extends Controller
{
    /**
     * Create a new controller instance.
     * constructor to check
     * 1. authentication
     * 2. user roles
     * 3. roles must be agent.
     *
     * @return void
     */
    public function __construct()
    {
        // checking for authentication
        $this->middleware('auth');
        // checking if the role is agent
        $this->middleware('role.agent');
    }

    /**
     * Get the Report page.
     *
     * @return type view
     */
    public function index()
    {
        try {
            return view('themes.default1.agent.helpdesk.report.index');
        } catch (Exception $e) {
        }
    }

    /**
     * function to get help_topic graph.
     *
     * @param type $date111
     * @param type $date122
     * @param type $helptopic
     */
    public function chartdataHelptopic(Request $request, $date111 = '', $date122 = '', $helptopic = '')
    {
        $date11 = strtotime($date122);

        $date12 = strtotime($date111);
        $help_topic = $helptopic;
        $duration = $request->input('duration');
        if ($date11 && $date12 && $help_topic) {
            $date2 = $date12;
            $date1 = $date11;
            $duration = null;
            if ($request->input('open') == null || $request->input('closed') == null || $request->input('reopened') == null || $request->input('overdue') == null || $request->input('deleted') == null) {
                $duration = 'day';
            }
        } else {
            // generating current date
            $date2 = strtotime(date('Y-m-d'));
            $date3 = date('Y-m-d');
            $format = 'Y-m-d';
            // generating a date range of 1 month
            if ($request->input('duration') == 'day') {
                $date1 = strtotime(date($format, strtotime('-15 day'.$date3)));
            } elseif ($request->input('duration') == 'week') {
                $date1 = strtotime(date($format, strtotime('-69 days'.$date3)));
            } elseif ($request->input('duration') == 'month') {
                $date1 = strtotime(date($format, strtotime('-179 days'.$date3)));
            } else {
                $date1 = strtotime(date($format, strtotime('-30 days'.$date3)));
            }
//            $help_topic = Help_topic::where('status', '=', '1')->min('id');
        }

        $return = '';
        $last = '';
        $j = 0;
        $created1 = 0;
        $closed1 = 0;
        $reopened1 = 0;
        $in_progress = \DB::table('tickets')->where('help_topic_id', '=', $help_topic)->where('status', '=', 1)->count();

        for ($i = $date1; $i <= $date2; $i = $i + 86400) {
            $j++;
            $thisDate = date('Y-m-d', $i);
            $thisDate1 = date('jS F', $i);
            $open_array = [];
            $closed_array = [];
            $reopened_array = [];

            if ($request->input('open') || $request->input('closed') || $request->input('reopened')) {
                if ($request->input('open') && $request->input('open') == 'on') {
                    $created = \DB::table('tickets')->select('created_at')->where('help_topic_id', '=', $help_topic)->where('created_at', 'LIKE', '%'.$thisDate.'%')->count();
                    $open_array = ['open' => $created];
                }
                if ($request->input('closed') && $request->input('closed') == 'on') {
                    $closed = \DB::table('tickets')->select('closed_at')->where('help_topic_id', '=', $help_topic)->where('closed_at', 'LIKE', '%'.$thisDate.'%')->count();
                    $closed_array = ['closed' => $closed];
                }
                if ($request->input('reopened') && $request->input('reopened') == 'on') {
                    $reopened = \DB::table('tickets')->select('reopened_at')->where('help_topic_id', '=', $help_topic)->where('reopened_at', 'LIKE', '%'.$thisDate.'%')->count();
                    $reopened_array = ['reopened' => $reopened];
                }
//                if ($request->input('overdue') && $request->input('overdue') == 'on') {
//                    $overdue = Tickets::where('status', '=', 1)->where('isanswered', '=', 0)->where('dept_id', '=', $dept->id)->orderBy('id', 'DESC')->get();
//                }
//                        $open_array = ['open'=>$created1];
//                        $closed_array = ['closed'=>$closed1];
//                        $reopened_array = ['reopened'=>$reopened1];
                $value = ['date' => $thisDate1];
//                        if($open_array) {
                $value = array_merge($value, $open_array);
                $value = array_merge($value, $closed_array);
                $value = array_merge($value, $reopened_array);
                $value = array_merge($value, ['inprogress' => $in_progress]);
//                        } else {
//                            $value = "";
//                        }
                $array = array_map('htmlentities', $value);
                $json = html_entity_decode(json_encode($array));
                $return .= $json.',';
            } else {
                if ($duration == 'week') {
                    $created = \DB::table('tickets')->select('created_at')->where('help_topic_id', '=', $help_topic)->where('created_at', 'LIKE', '%'.$thisDate.'%')->count();
                    $created1 += $created;
                    $closed = \DB::table('tickets')->select('closed_at')->where('help_topic_id', '=', $help_topic)->where('closed_at', 'LIKE', '%'.$thisDate.'%')->count();
                    $closed1 += $closed;
                    $reopened = \DB::table('tickets')->select('reopened_at')->where('help_topic_id', '=', $help_topic)->where('reopened_at', 'LIKE', '%'.$thisDate.'%')->count();
                    $reopened1 += $reopened;
                    if ($j % 7 == 0) {
                        $open_array = ['open' => $created1];
                        $created1 = 0;
                        $closed_array = ['closed' => $closed1];
                        $closed1 = 0;
                        $reopened_array = ['reopened' => $reopened1];
                        $reopened1 = 0;
                        $value = ['date' => $thisDate1];
//                        if($open_array) {
                        $value = array_merge($value, $open_array);
                        $value = array_merge($value, $closed_array);
                        $value = array_merge($value, $reopened_array);
                        $value = array_merge($value, ['inprogress' => $in_progress]);
//                        } else {
//                            $value = "";
//                        }
                        $array = array_map('htmlentities', $value);
                        $json = html_entity_decode(json_encode($array));
                        $return .= $json.',';
                    }
                } elseif ($duration == 'month') {
                    $created_month = \DB::table('tickets')->select('created_at')->where('help_topic_id', '=', $help_topic)->where('created_at', 'LIKE', '%'.$thisDate.'%')->count();
                    $created1 += $created_month;
                    $closed_month = \DB::table('tickets')->select('closed_at')->where('help_topic_id', '=', $help_topic)->where('closed_at', 'LIKE', '%'.$thisDate.'%')->count();
                    $closed1 += $closed_month;
                    $reopened_month = \DB::table('tickets')->select('reopened_at')->where('help_topic_id', '=', $help_topic)->where('reopened_at', 'LIKE', '%'.$thisDate.'%')->count();
                    $reopened1 += $reopened_month;
                    if ($j % 30 == 0) {
                        $open_array = ['open' => $created1];
                        $created1 = 0;
                        $closed_array = ['closed' => $closed1];
                        $closed1 = 0;
                        $reopened_array = ['reopened' => $reopened1];
                        $reopened1 = 0;
                        $value = ['date' => $thisDate1];

                        $value = array_merge($value, $open_array);
                        $value = array_merge($value, $closed_array);
                        $value = array_merge($value, $reopened_array);
                        $value = array_merge($value, ['inprogress' => $in_progress]);

                        $array = array_map('htmlentities', $value);
                        $json = html_entity_decode(json_encode($array));
                        $return .= $json.',';
                    }
                } else {
                    if ($request->input('default') == null) {
                        $help_topic = Help_topic::where('status', '=', '1')->min('id');
                    }
                    $created = \DB::table('tickets')->select('created_at')->where('help_topic_id', '=', $help_topic)->where('created_at', 'LIKE', '%'.$thisDate.'%')->count();
                    $open_array = ['open' => $created];
                    $closed = \DB::table('tickets')->select('closed_at')->where('help_topic_id', '=', $help_topic)->where('closed_at', 'LIKE', '%'.$thisDate.'%')->count();
                    $closed_array = ['closed' => $closed];
                    $reopened = \DB::table('tickets')->select('reopened_at')->where('help_topic_id', '=', $help_topic)->where('reopened_at', 'LIKE', '%'.$thisDate.'%')->count();
                    $reopened_array = ['reopened' => $reopened];
                    if ($j % 1 == 0) {
                        $open_array = ['open' => $created];
                        $created = 0;
                        $closed_array = ['closed' => $closed];
                        $closed = 0;
                        $reopened_array = ['reopened' => $reopened];
                        $reopened = 0;
                        $value = ['date' => $thisDate1];
                        if ($request->input('default') == null) {
                            $value = array_merge($value, $open_array);
                            $value = array_merge($value, $closed_array);
                            $value = array_merge($value, $reopened_array);
                            $value = array_merge($value, ['inprogress' => $in_progress]);
                        } else {
                            if ($duration == null) {
                                if ($request->input('open') == 'on') {
                                    $value = array_merge($value, $open_array);
                                }
                                if ($request->input('closed') == 'on') {
                                    $value = array_merge($value, $closed_array);
                                }
                                if ($request->input('reopened') == 'on') {
                                    $value = array_merge($value, $reopened_array);
                                }
                            } else {
                                $value = array_merge($value, $open_array);
                                $value = array_merge($value, $closed_array);
                                $value = array_merge($value, $reopened_array);
                                $value = array_merge($value, ['inprogress' => $in_progress]);
                            }
                        }

//                        } else {
//                            $value = "";
//                        }
                        $array = array_map('htmlentities', $value);
                        $json = html_entity_decode(json_encode($array));
                        $return .= $json.',';
                    }
                }
            } 
        }
        $last = rtrim($return, ',');

        return '['.$last.']';
    }

    public function helptopicPdf(Request $request)
    {
        $table_datas = json_decode($request->input('pdf_form'));
        $table_help_topic = json_decode($request->input('pdf_form_help_topic'));
        $html = view('themes.default1.agent.helpdesk.report.pdf', compact('table_datas', 'table_help_topic'))->render();
        $html1 = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');

        return PDF::load($html1)->show();
    }

    function report(Request $request)
    {

    $results = DB::select( DB::raw("SELECT DISTINCT
    r0.ticket_id AS 'Ticket',
    r5.ticket_number AS 'Reference',
   r1.content AS 'Patient',
   r2.content AS 'Member_No',
   r6.title AS 'Subject',
   r5.created_at AS 'Date_Logged',
   r7.user_name AS 'Logged_By',
   r8.user AS 'Escalated_to',
   r9.name AS 'Service',
   r10.name AS 'Service_Issue',
   CASE
       WHEN r5.reopened = 1 THEN 'Re-Assigned'
       ELSE ''
   END AS 'Re_Assigned',
   CASE
       WHEN r5.closed = 1 THEN 'Closed'
       ELSE ''
   END AS 'Closed',
   CASE
       WHEN r5.closed_at != ''  THEN r5.closed_at
       ELSE ''
   END AS 'Date_Closed' ,
   r3.content AS 'Location_Name',
   r4.content AS 'Scheme',
   r11.name AS 'Reported_Via'
FROM
   (SELECT DISTINCT
       ticket_id
   FROM
       ticket_form_data) r0
       LEFT JOIN
   ticket_form_data r1 ON r1.ticket_id = r0.ticket_id
       AND r1.title = 'patientfullname'
       LEFT JOIN
   ticket_form_data r2 ON r2.ticket_id = r0.ticket_id
       AND r2.title = 'memberno'
       LEFT JOIN
   ticket_form_data r3 ON r3.ticket_id = r0.ticket_id
       AND r3.title = 'locationname'
       LEFT JOIN
   ticket_form_data r4 ON r4.ticket_id = r0.ticket_id
       AND r4.title = 'accountfullname'
       LEFT JOIN
   tickets r5 ON r5.id = r0.ticket_id
       LEFT JOIN
   ticket_thread r6 ON r6.ticket_id = r0.ticket_id
       LEFT JOIN
   users r7 ON r7.id = r5.user_id
       LEFT JOIN
   user_escalations r8 ON r8.id = r5.assigned_to
       LEFT JOIN
   services r9 ON r9.id = r5.service
       LEFT JOIN
   service_issues r10 ON r10.id = r5.service_issue
   LEFT JOIN
   report_channels r11 ON r11.id = r5.reported_via 
   where  r6.title !=''
ORDER BY r0.ticket_id") ); 
// dd($request->get('start_date'));
$items = $results; // get array/collection data
$data = $this->getPaginator($request, $items);
 
 
return view('themes.default1.agent.helpdesk.report.index')->with('items',$data); 
    } 


    private function getPaginator(Request $request, $items)
    {
        $total = count($items); // total count of the set, this is necessary so the paginator will know the total pages to display
        $page = $request->page ?? 1; // get current page from the request, first page is null
        $perPage = 5; // how many items you want to display per page?
        $offset = ($page - 1) * $perPage; // get the offset, how many items need to be "skipped" on this page
        $items = array_slice($items, $offset, $perPage); // the array that we actually pass to the paginator is sliced
    
        return new LengthAwarePaginator($items, $total, $perPage, $page, [
            'path' => $request->url(),
            'query' => $request->query()
        ]);
    } 
    public function export(Request $request, $type) {
        $items = DB::select( DB::raw("SELECT DISTINCT
        r0.ticket_id AS 'Ticket',
        r5.ticket_number AS 'Reference',
       r1.content AS 'Patient',
       r2.content AS 'Member_No',
       r6.title AS 'Subject',
       r5.created_at AS 'Date_Logged',
       r7.user_name AS 'Logged_By',
       r8.user AS 'Escalated_to',
       r9.name AS 'Service',
       r10.name AS 'Service_Issue',
       CASE
           WHEN r5.reopened = 1 THEN 'Re-Assigned'
           ELSE ''
       END AS 'Re_Assigned',
       CASE
           WHEN r5.closed = 1 THEN 'Closed'
           ELSE ''
       END AS 'Closed',
       CASE
           WHEN r5.closed_at != ''  THEN r5.closed_at
           ELSE ''
       END AS 'Date_Closed' ,
       r3.content AS 'Location_Name',
       r4.content AS 'Scheme',
       r11.name AS 'Reported_Via'
    FROM
       (SELECT DISTINCT
           ticket_id
       FROM
           ticket_form_data) r0
           LEFT JOIN
       ticket_form_data r1 ON r1.ticket_id = r0.ticket_id
           AND r1.title = 'patientfullname'
           LEFT JOIN
       ticket_form_data r2 ON r2.ticket_id = r0.ticket_id
           AND r2.title = 'memberno'
           LEFT JOIN
       ticket_form_data r3 ON r3.ticket_id = r0.ticket_id
           AND r3.title = 'locationname'
           LEFT JOIN
       ticket_form_data r4 ON r4.ticket_id = r0.ticket_id
           AND r4.title = 'accountfullname'
           LEFT JOIN
       tickets r5 ON r5.id = r0.ticket_id
           LEFT JOIN
       ticket_thread r6 ON r6.ticket_id = r0.ticket_id
           LEFT JOIN
       users r7 ON r7.id = r5.user_id
           LEFT JOIN
       user_escalations r8 ON r8.id = r5.assigned_to
           LEFT JOIN
       services r9 ON r9.id = r5.service
           LEFT JOIN
       service_issues r10 ON r10.id = r5.service_issue
       LEFT JOIN
       report_channels r11 ON r11.id = r5.reported_via
       where r6.title !='' 
    ORDER BY r0.ticket_id") ); 
    // dd($employees); 
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet(); 
        $spreadsheet->getActiveSheet()->mergeCells("A1:P1")->setCellValue('A1','Master CMT Report'); 
        $sheet->setCellValue('A2', 'Ticket');
        $sheet->setCellValue('B2', 'Reference');
        $sheet->setCellValue('C2', 'Patient Name');
        $sheet->setCellValue('D2', 'Member No');
        $sheet->setCellValue('E2', 'Subject');
        $sheet->setCellValue('F2', 'Date Logged');
        $sheet->setCellValue('G2', 'Logged By');
        $sheet->setCellValue('H2', 'Escalated To');
        $sheet->setCellValue('I2', 'Service');
        $sheet->setCellValue('J2', 'Service Issue');
        $sheet->setCellValue('K2', 'Re-Assigned');
        $sheet->setCellValue('L2', 'Closed');
        $sheet->setCellValue('M2', 'Date Closed');
        $sheet->setCellValue('N2', 'Location Name');
        $sheet->setCellValue('O2', 'Scheme');
        $sheet->setCellValue('P2', 'Reported Via'); 
        $rows = 3;
        foreach($items as $empDetails){
        $sheet->setCellValue('A' . $rows, $empDetails->Ticket);
        $sheet->setCellValue('B' . $rows, $empDetails->Reference);
        $sheet->setCellValue('C' . $rows, $empDetails->Patient);
        $sheet->setCellValue('D' . $rows, $empDetails->Member_No);
        $sheet->setCellValue('E' . $rows, $empDetails->Subject);   
        $sheet->setCellValue('F' . $rows, $empDetails->Date_Logged);
        $sheet->setCellValue('G' . $rows, $empDetails->Logged_By); 
        $sheet->setCellValue('H' . $rows, $empDetails->Escalated_to);
        $sheet->setCellValue('I' . $rows, $empDetails->Service); 
        $sheet->setCellValue('J' . $rows, $empDetails->Service_Issue);
        $sheet->setCellValue('K' . $rows, $empDetails->Re_Assigned); 
        $sheet->setCellValue('L' . $rows, $empDetails->Closed);
        $sheet->setCellValue('M' . $rows, $empDetails->Date_Closed); 
        $sheet->setCellValue('N' . $rows, $empDetails->Location_Name);
        $sheet->setCellValue('O' . $rows, $empDetails->Scheme); 
        $sheet->setCellValue('P' . $rows, $empDetails->Reported_Via); 
        $rows++;
        }
        $now = time(); 
        $fileName = "DumpMaster".$now.".".$type;
        if($type == 'xlsx') {
        $writer = new Xlsx($spreadsheet);
        } else if($type == 'xls') {
        $writer = new Xls($spreadsheet);
        } 
         
        // $writer->save($fileName); 
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'. urlencode($fileName).'"');
        $writer->save('php://output');
        exit(); 
        }

        public function departmentreport(Request $request, $type) {
         

            $visitorTraffic =  DB::select( DB::raw( "
            SELECT 
                services.name AS service,
                COUNT(CASE
                    WHEN
                        tickets.created_at >= DATE_ADD(CURDATE(), INTERVAL - 7 DAY)
                            AND status = 1
                    THEN
                        ticket_number
                END) first,
                COUNT(CASE
                    WHEN
                        tickets.created_at <= DATE_SUB(CURDATE(), INTERVAL 7 DAY)
                            AND status = 1
                            AND tickets.created_at >= DATE_SUB(CURDATE(), INTERVAL 15 DAY)
                            AND status = 1
                    THEN
                        ticket_number
                END) two,
                COUNT(CASE
                    WHEN
                        tickets.created_at <= DATE_SUB(CURDATE(), INTERVAL 15 DAY)
                            AND status = 1
                            AND tickets.created_at >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)
                            AND status = 1
                    THEN
                        ticket_number
                END) third,
                COUNT(CASE
                    WHEN
                        tickets.created_at <= DATE_SUB(CURDATE(), INTERVAL 30 DAY)
                            AND status = 1
                            AND tickets.created_at >= DATE_SUB(CURDATE(), INTERVAL 365 DAY)
                            AND status = 1
                    THEN
                        ticket_number
                END) fourth,
                COUNT(CASE
                    WHEN status = 1 THEN ticket_number
                END) total
            FROM
                `tickets`
                    LEFT JOIN
                services ON tickets.service = services.id
            GROUP BY services.name 
            UNION (SELECT 
                'Grand Total' AS product,
                COUNT(CASE
                    WHEN
                        tickets.created_at >= DATE_ADD(CURDATE(), INTERVAL - 7 DAY)
                            AND status = 1
                    THEN
                        ticket_number
                END) AS first,
                COUNT(CASE
                    WHEN
                        tickets.created_at <= DATE_SUB(CURDATE(), INTERVAL 7 DAY)
                            AND status = 1
                            AND tickets.created_at >= DATE_SUB(CURDATE(), INTERVAL 15 DAY)
                            AND status = 1
                    THEN
                        ticket_number
                END) AS two,
                COUNT(CASE
                    WHEN
                        tickets.created_at <= DATE_SUB(CURDATE(), INTERVAL 15 DAY)
                            AND status = 1
                            AND tickets.created_at >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)
                            AND status = 1
                    THEN
                        ticket_number
                END) AS third,
                COUNT(CASE
                    WHEN
                        tickets.created_at <= DATE_SUB(CURDATE(), INTERVAL 30 DAY)
                            AND status = 1
                            AND tickets.created_at >= DATE_SUB(CURDATE(), INTERVAL 365 DAY)
                            AND status = 1
                    THEN
                        ticket_number
                END) AS fourth,
                COUNT(CASE
                    WHEN status = 1 THEN ticket_number
                END) AS total
            FROM
                tickets)
            ;"));
            
     $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $spreadsheet->getActiveSheet()->mergeCells("A1:F1")->setCellValue('A1','Patient Complaint'); 
        $spreadsheet->getActiveSheet()->mergeCells("A2:F2")->setCellValue('A2','Pending Escalations by Departments');
        $spreadsheet->getActiveSheet()->mergeCells("B3:E3")->setCellValue('B3','Open Issues');  
        $sheet->setCellValue('A4', 'Department');
        $sheet->setCellValue('B4', '<7 Days');
        $sheet->setCellValue('C4', '7-15 Days');
        $sheet->setCellValue('D4', '16-30 Days');
        $sheet->setCellValue('E4', '>30 Days');
        $sheet->setCellValue('F4', 'Total Open Issues'); 
        $rows = 5;
        foreach($visitorTraffic as $empDetails){
        $sheet->setCellValue('A' . $rows, $empDetails->service);
        $sheet->setCellValue('B' . $rows, $empDetails->first);
        $sheet->setCellValue('C' . $rows, $empDetails->two);
        $sheet->setCellValue('D' . $rows, $empDetails->third);
        $sheet->setCellValue('E' . $rows, $empDetails->fourth); 
        $sheet->setCellValue('F' . $rows, $empDetails->total);
        
        $rows++;
        }  
        $fileName = "Department_Report.".$type;
        if($type == 'xlsx') {
        $writer = new Xlsx($spreadsheet);
        } else if($type == 'xls') {
        $writer = new Xls($spreadsheet);
        } 
         
        // $writer->save($fileName); 
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'. urlencode($fileName).'"');
        $writer->save('php://output');
        exit();
        // header("Content-Type: application/vnd.ms-excel");  
         
        // return redirect(url('report'));
         
        }

        public function regionsreport(Request $request, $type) {
         

            $visitorTraffic =  DB::select( DB::raw( "
            SELECT 
                facilities.name AS region,
                COUNT(CASE
                    WHEN
                        tickets.created_at >= DATE_ADD(CURDATE(), INTERVAL - 7 DAY)
                            AND status = 1
                    THEN
                        ticket_number
                END) first,
                COUNT(CASE
                    WHEN
                        tickets.created_at <= DATE_SUB(CURDATE(), INTERVAL 7 DAY)
                            AND status = 1
                            AND tickets.created_at >= DATE_SUB(CURDATE(), INTERVAL 15 DAY)
                            AND status = 1
                    THEN
                        ticket_number
                END) two,
                COUNT(CASE
                    WHEN
                        tickets.created_at <= DATE_SUB(CURDATE(), INTERVAL 15 DAY)
                            AND status = 1
                            AND tickets.created_at >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)
                            AND status = 1
                    THEN
                        ticket_number
                END) third,
                COUNT(CASE
                    WHEN
                        tickets.created_at <= DATE_SUB(CURDATE(), INTERVAL 30 DAY)
                            AND status = 1
                            AND tickets.created_at >= DATE_SUB(CURDATE(), INTERVAL 365 DAY)
                            AND status = 1
                    THEN
                        ticket_number
                END) fourth,
                COUNT(CASE
                    WHEN status = 1 THEN ticket_number
                END) total
            FROM
                `tickets`
                    LEFT JOIN
                facilities ON tickets.facility = facilities.id
            GROUP BY facilities.name 
            UNION (SELECT 
                'Grand Total' AS product,
                COUNT(CASE
                    WHEN
                        tickets.created_at >= DATE_ADD(CURDATE(), INTERVAL - 7 DAY)
                            AND status = 1
                    THEN
                        ticket_number
                END) AS first,
                COUNT(CASE
                    WHEN
                        tickets.created_at <= DATE_SUB(CURDATE(), INTERVAL 7 DAY)
                            AND status = 1
                            AND tickets.created_at >= DATE_SUB(CURDATE(), INTERVAL 15 DAY)
                            AND status = 1
                    THEN
                        ticket_number
                END) AS two,
                COUNT(CASE
                    WHEN
                        tickets.created_at <= DATE_SUB(CURDATE(), INTERVAL 15 DAY)
                            AND status = 1
                            AND tickets.created_at >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)
                            AND status = 1
                    THEN
                        ticket_number
                END) AS third,
                COUNT(CASE
                    WHEN
                        tickets.created_at <= DATE_SUB(CURDATE(), INTERVAL 30 DAY)
                            AND status = 1
                            AND tickets.created_at >= DATE_SUB(CURDATE(), INTERVAL 365 DAY)
                            AND status = 1
                    THEN
                        ticket_number
                END) AS fourth,
                COUNT(CASE
                    WHEN status = 1 THEN ticket_number
                END) AS total
            FROM
                tickets)
            ;"));
            
     $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $spreadsheet->getActiveSheet()->mergeCells("A1:F1")->setCellValue('A1','Patient Complaint'); 
        $spreadsheet->getActiveSheet()->mergeCells("A2:F2")->setCellValue('A2','Pending Escalations by Regions');
        $spreadsheet->getActiveSheet()->mergeCells("B3:E3")->setCellValue('B3','Open Issues');  
        $sheet->setCellValue('A4', 'Region');
        $sheet->setCellValue('B4', '<7 Days');
        $sheet->setCellValue('C4', '7-15 Days');
        $sheet->setCellValue('D4', '16-30 Days');
        $sheet->setCellValue('E4', '>30 Days');
        $sheet->setCellValue('F4', 'Total Open Issues'); 
        $rows = 5;
        foreach($visitorTraffic as $empDetails){
        $sheet->setCellValue('A' . $rows, $empDetails->region);
        $sheet->setCellValue('B' . $rows, $empDetails->first);
        $sheet->setCellValue('C' . $rows, $empDetails->two);
        $sheet->setCellValue('D' . $rows, $empDetails->third);
        $sheet->setCellValue('E' . $rows, $empDetails->fourth); 
        $sheet->setCellValue('F' . $rows, $empDetails->total);
        
        $rows++;
        }  
        $fileName = "Regions_Report.".$type;
        if($type == 'xlsx') {
        $writer = new Xlsx($spreadsheet);
        } else if($type == 'xls') {
        $writer = new Xls($spreadsheet);
        } 
         
        // $writer->save($fileName); 
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'. urlencode($fileName).'"');
        $writer->save('php://output');
        exit();
        // header("Content-Type: application/vnd.ms-excel");  
         
        // return redirect(url('report'));
         
        }
        public function departmentreportclosed(Request $request, $type) {
         

            $visitorTraffic =  DB::select( DB::raw( "
            SELECT 
                services.name AS service,
                COUNT(CASE
                    WHEN
                        tickets.created_at >= DATE_ADD(CURDATE(), INTERVAL - 7 DAY)
                            AND status != 1
                    THEN
                        ticket_number
                END) first,
                COUNT(CASE
                    WHEN
                        tickets.created_at <= DATE_SUB(CURDATE(), INTERVAL 7 DAY)
                            AND status != 1
                            AND tickets.created_at >= DATE_SUB(CURDATE(), INTERVAL 15 DAY)
                            AND status != 1
                    THEN
                        ticket_number
                END) two,
                COUNT(CASE
                    WHEN
                        tickets.created_at <= DATE_SUB(CURDATE(), INTERVAL 15 DAY)
                            AND status != 1
                            AND tickets.created_at >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)
                            AND status != 1
                    THEN
                        ticket_number
                END) third,
                COUNT(CASE
                    WHEN
                        tickets.created_at <= DATE_SUB(CURDATE(), INTERVAL 30 DAY)
                            AND status != 1
                            AND tickets.created_at >= DATE_SUB(CURDATE(), INTERVAL 365 DAY)
                            AND status != 1
                    THEN
                        ticket_number
                END) fourth,
                COUNT(CASE
                    WHEN status != 1 THEN ticket_number
                END) total
            FROM
                `tickets`
                    LEFT JOIN
                services ON tickets.service = services.id
            GROUP BY services.name 
            UNION (SELECT 
                'Grand Total' AS product,
                COUNT(CASE
                    WHEN
                        tickets.created_at >= DATE_ADD(CURDATE(), INTERVAL - 7 DAY)
                            AND status != 1
                    THEN
                        ticket_number
                END) AS first,
                COUNT(CASE
                    WHEN
                        tickets.created_at <= DATE_SUB(CURDATE(), INTERVAL 7 DAY)
                            AND status != 1
                            AND tickets.created_at >= DATE_SUB(CURDATE(), INTERVAL 15 DAY)
                            AND status != 1
                    THEN
                        ticket_number
                END) AS two,
                COUNT(CASE
                    WHEN
                        tickets.created_at <= DATE_SUB(CURDATE(), INTERVAL 15 DAY)
                            AND status != 1
                            AND tickets.created_at >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)
                            AND status != 1
                    THEN
                        ticket_number
                END) AS third,
                COUNT(CASE
                    WHEN
                        tickets.created_at <= DATE_SUB(CURDATE(), INTERVAL 30 DAY)
                            AND status != 1
                            AND tickets.created_at >= DATE_SUB(CURDATE(), INTERVAL 365 DAY)
                            AND status != 1
                    THEN
                        ticket_number
                END) AS fourth,
                COUNT(CASE
                    WHEN status != 1 THEN ticket_number
                END) AS total
            FROM
                tickets)
            ;"));
            
     $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $spreadsheet->getActiveSheet()->mergeCells("A1:F1")->setCellValue('A1','Patient Complaint'); 
        $spreadsheet->getActiveSheet()->mergeCells("A2:F2")->setCellValue('A2','Escalations by Departments');
        $spreadsheet->getActiveSheet()->mergeCells("B3:E3")->setCellValue('B3','Closed Issues');  
        $sheet->setCellValue('A4', 'Department');
        $sheet->setCellValue('B4', '<7 Days');
        $sheet->setCellValue('C4', '7-15 Days');
        $sheet->setCellValue('D4', '16-30 Days');
        $sheet->setCellValue('E4', '>30 Days');
        $sheet->setCellValue('F4', 'Total Closed Issues'); 
        $rows = 5;
        foreach($visitorTraffic as $empDetails){
        $sheet->setCellValue('A' . $rows, $empDetails->service);
        $sheet->setCellValue('B' . $rows, $empDetails->first);
        $sheet->setCellValue('C' . $rows, $empDetails->two);
        $sheet->setCellValue('D' . $rows, $empDetails->third);
        $sheet->setCellValue('E' . $rows, $empDetails->fourth); 
        $sheet->setCellValue('F' . $rows, $empDetails->total);
        
        $rows++;
        }  
        $fileName = "Department_Report_Closed.".$type;
        if($type == 'xlsx') {
        $writer = new Xlsx($spreadsheet);
        } else if($type == 'xls') {
        $writer = new Xls($spreadsheet);
        } 
         
        // $writer->save($fileName); 
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'. urlencode($fileName).'"');
        $writer->save('php://output');
        exit();
        // header("Content-Type: application/vnd.ms-excel");  
         
        // return redirect(url('report'));
         
        }

        public function regionsreportclosed(Request $request, $type) {
         

            $visitorTraffic =  DB::select( DB::raw( "
            SELECT 
                facilities.name AS region,
                COUNT(CASE
                    WHEN
                        tickets.created_at >= DATE_ADD(CURDATE(), INTERVAL - 7 DAY)
                            AND status != 1
                    THEN
                        ticket_number
                END) first,
                COUNT(CASE
                    WHEN
                        tickets.created_at <= DATE_SUB(CURDATE(), INTERVAL 7 DAY)
                            AND status != 1
                            AND tickets.created_at >= DATE_SUB(CURDATE(), INTERVAL 15 DAY)
                            AND status != 1
                    THEN
                        ticket_number
                END) two,
                COUNT(CASE
                    WHEN
                        tickets.created_at <= DATE_SUB(CURDATE(), INTERVAL 15 DAY)
                            AND status != 1
                            AND tickets.created_at >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)
                            AND status != 1
                    THEN
                        ticket_number
                END) third,
                COUNT(CASE
                    WHEN
                        tickets.created_at <= DATE_SUB(CURDATE(), INTERVAL 30 DAY)
                            AND status != 1
                            AND tickets.created_at >= DATE_SUB(CURDATE(), INTERVAL 365 DAY)
                            AND status != 1
                    THEN
                        ticket_number
                END) fourth,
                COUNT(CASE
                    WHEN status != 1 THEN ticket_number
                END) total
            FROM
                `tickets`
                    LEFT JOIN
                facilities ON tickets.facility = facilities.id
            GROUP BY facilities.name 
            UNION (SELECT 
                'Grand Total' AS product,
                COUNT(CASE
                    WHEN
                        tickets.created_at >= DATE_ADD(CURDATE(), INTERVAL - 7 DAY)
                            AND status != 1
                    THEN
                        ticket_number
                END) AS first,
                COUNT(CASE
                    WHEN
                        tickets.created_at <= DATE_SUB(CURDATE(), INTERVAL 7 DAY)
                            AND status != 1
                            AND tickets.created_at >= DATE_SUB(CURDATE(), INTERVAL 15 DAY)
                            AND status != 1
                    THEN
                        ticket_number
                END) AS two,
                COUNT(CASE
                    WHEN
                        tickets.created_at <= DATE_SUB(CURDATE(), INTERVAL 15 DAY)
                            AND status != 1
                            AND tickets.created_at >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)
                            AND status != 1
                    THEN
                        ticket_number
                END) AS third,
                COUNT(CASE
                    WHEN
                        tickets.created_at <= DATE_SUB(CURDATE(), INTERVAL 30 DAY)
                            AND status != 1
                            AND tickets.created_at >= DATE_SUB(CURDATE(), INTERVAL 365 DAY)
                            AND status != 1
                    THEN
                        ticket_number
                END) AS fourth,
                COUNT(CASE
                    WHEN status != 1 THEN ticket_number
                END) AS total
            FROM
                tickets)
            ;"));
            
     $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $spreadsheet->getActiveSheet()->mergeCells("A1:F1")->setCellValue('A1','Patient Complaint'); 
        $spreadsheet->getActiveSheet()->mergeCells("A2:F2")->setCellValue('A2','Escalations by Regions');
        $spreadsheet->getActiveSheet()->mergeCells("B3:E3")->setCellValue('B3','Closed Issues');  
        $sheet->setCellValue('A4', 'Region');
        $sheet->setCellValue('B4', '<7 Days');
        $sheet->setCellValue('C4', '7-15 Days');
        $sheet->setCellValue('D4', '16-30 Days');
        $sheet->setCellValue('E4', '>30 Days');
        $sheet->setCellValue('F4', 'Total Closed Issues'); 
        $rows = 5;
        foreach($visitorTraffic as $empDetails){
        $sheet->setCellValue('A' . $rows, $empDetails->region);
        $sheet->setCellValue('B' . $rows, $empDetails->first);
        $sheet->setCellValue('C' . $rows, $empDetails->two);
        $sheet->setCellValue('D' . $rows, $empDetails->third);
        $sheet->setCellValue('E' . $rows, $empDetails->fourth); 
        $sheet->setCellValue('F' . $rows, $empDetails->total);
        
        $rows++;
        }  
        $fileName = "Regions_Report_Closed.".$type;
        if($type == 'xlsx') {
        $writer = new Xlsx($spreadsheet);
        } else if($type == 'xls') {
        $writer = new Xls($spreadsheet);
        } 
         
        // $writer->save($fileName); 
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'. urlencode($fileName).'"');
        $writer->save('php://output');
        exit();
        // header("Content-Type: application/vnd.ms-excel");  
         
        // return redirect(url('report'));
         
        }
    }