<?php

namespace App\Http\Controllers\Admin\helpdesk;

use App\Model\Bliss\Service;
use App\Model\Bliss\ServiceIssue;
use App\Model\Bliss\UserEscalation;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Redirect,Response;

class EscalationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::with('serviceissues.escalations.region')->get();
        try {
            return view('themes.default1.admin.helpdesk.escalation.index', compact('services'));
        } catch (Exception $e) {
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {   
        $where = array('id' => $id);
        $service  = Service::where($where)->first();
        return redirect()->back()->with('success', "service issue EDITED");
        // return Response::json($service);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, $id)
    // {
    //     //
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function createService(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('fails', "Enter required fields correctly");
        }

        $service = Service::firstOrCreate($request->except('_token'));
        return $this->index();
    }

    public function createServiceIssue(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'service_id' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('fails', "Enter required fields correctly");
        }
        $service = ServiceIssue::firstOrCreate($request->except('_token'));
        return $this->index();
    }

    public function setEscalation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user' => 'required|email',
            'service_issue' => 'required|numeric',
            'region_id' => 'required|numeric',
            'escalation_level' => 'required|numeric',
            'minutes' => 'required|numeric',
            'company' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('fails', "Enter required fields correctly");
        }
        $service = UserEscalation::updateOrCreate([
            'service_issue_id' => $request->service_issue,
            'region_id' => $request->region_id,
            'escalation_level' => $request->escalation_level,
            'company_id' => $request->company
        ], [
            'user' => $request->user,
            'minutes' => $request->minutes
        ]);
        return $this->index();
    }

    public function deleteServiceIssue($id){
        UserEscalation::where('service_issue_id',$id)->delete();
        ServiceIssue::where('id',$id)->delete();
        return redirect()->back()->with('success', "service issue deleted");
    }

    public function deleteService($id){
        $serviceIssues=ServiceIssue::where('service_id',$id)->get();
        foreach ($serviceIssues as $serviceIssue){
            UserEscalation::where('service_issue_id',$serviceIssue->id)->delete();
            ServiceIssue::where('id',$serviceIssue->id)->delete();
        }
        Service::where('id',$id)->delete();
        return redirect()->back()->with('success', "service deleted");
    }
    
}
