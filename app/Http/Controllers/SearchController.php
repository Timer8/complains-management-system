<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class SearchController extends Controller
{
    // public function index()
    // {
    //     $data = \DB::connection('externaldb')->table('master_visitentry')->paginate(10);
    //     return view('search', compact('data'));
    // }
    public function simple(Request $request)
    {
        $data = \DB::connection('externaldb')->table('master_visitentry');
        if( $request->input('search')){
            $data = $data->where('patientfullname', 'LIKE', "%" . $request->search . "%");
        }
        $data = $data->paginate(10);
        return view('search', compact('data'));
    }
    public function advance(Request $request)
    {
        $data = \DB::connection('externaldb')->table('master_visitentry');
        if( $request->patientfullname){
            $data = $data->where('patientfullname', 'LIKE', "%" . $request->patientfullname . "%");
        }
        if( $request->patientcode){
            $data = $data->where('patientcode', 'LIKE', "%" . $request->patientcode . "%");
        }
        // if( $request->min_age && $request->max_age ){
        //     $data = $data->where('age', '>=', $request->min_age)
        //                  ->where('age', '<=', $request->max_age);
        // }
        $data = $data->paginate(10);
        return view('search', compact('data'));
    }
}
