<?php

use App\Model\Bliss\Facility;
use App\Model\Bliss\Region;
use App\Model\Bliss\Company;
use App\Model\Bliss\ReportChannel;
use App\Model\Bliss\Service;
use App\Model\Bliss\ServiceIssue;
use App\Model\Bliss\UserEscalation;
use App\User;
use Illuminate\Database\Seeder;

class BlissSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Seed regions
        $regions = array(
            array('id' => 1, "name" => "Central"),
            array('id' => 2, "name" => "Coast"),
            array('id' => 3, "name" => "Nairobi Central"),
            array('id' => 4, "name" => "Nairobi North"),
            array('id' => 5, "name" => "Nairobi South"),
            array('id' => 6, "name" => "Nyanza"),
            array('id' => 7, "name" => "Rift Valley"),
            array('id' => 8, "name" => "Western"),
        );

        foreach ($regions as $region) {
            Region::updateOrCreate($region);
        }


        //Seed facilities
        $facilities = array(
            array("name" => "Embu", "company_id" => 1, "region_id" => 1, "business_filter" => 1),
            array("name" => "Embu  MediCross", "company_id" => 1, "region_id" => 1, "business_filter" => 1),
            array("name" => "Kerugoya", "company_id" => 1, "region_id" => 1, "business_filter" => 1),
            array("name" => "Marsabit", "company_id" => 1, "region_id" => 1, "business_filter" => 1),
            array("name" => "Maua", "company_id" => 1, "region_id" => 1, "business_filter" => 1),
            array("name" => "Meru", "company_id" => 1, "region_id" => 1, "business_filter" => 1),
            array("name" => "Meru  MediCross", "company_id" => 1, "region_id" => 1, "business_filter" => 1),
            array("name" => "Muranga", "company_id" => 1, "region_id" => 1, "business_filter" => 1),
            array("name" => "Nanyuki", "company_id" => 1, "region_id" => 1, "business_filter" => 1),
            array("name" => "Nkubu", "company_id" => 1, "region_id" => 1, "business_filter" => 1),
            array("name" => "Nyeri 1", "company_id" => 1, "region_id" => 1, "business_filter" => 1),
            array("name" => "Nyeri 2", "company_id" => 1, "region_id" => 1, "business_filter" => 1),

            array("name" => "Diani", "company_id" => 1, "region_id" => 2, "business_filter" => 1),
            array("name" => "Emali", "company_id" => 1, "region_id" => 2, "business_filter" => 1),
            array("name" => "Jubilee MediCross", "company_id" => 1, "region_id" => 2, "business_filter" => 1),
            array("name" => "Kilifi  MediCross", "company_id" => 1, "region_id" => 2, "business_filter" => 1),
            array("name" => "Malindi MediCross", "company_id" => 1, "region_id" => 2, "business_filter" => 1),
            array("name" => "Mombasa Kizingo", "company_id" => 1, "region_id" => 2, "business_filter" => 1),
            array("name" => "Mtwapa", "company_id" => 1, "region_id" => 2, "business_filter" => 1),

            array("name" => "Embakasi", "company_id" => 1, "region_id" => 3, "business_filter" => 1),
            array("name" => "Garissa", "company_id" => 1, "region_id" => 3, "business_filter" => 1),
            array("name" => "Jogoo Road", "company_id" => 1, "region_id" => 3, "business_filter" => 1),
            array("name" => "Kajiado", "company_id" => 1, "region_id" => 3, "business_filter" => 1),
            array("name" => "Kawangware  MediCross", "company_id" => 1, "region_id" => 3, "business_filter" => 1),
            array("name" => "Kibera", "company_id" => 1, "region_id" => 3, "business_filter" => 1),
            array("name" => "Kitui", "company_id" => 1, "region_id" => 3, "business_filter" => 1),
            array("name" => "Mfi", "company_id" => 1, "region_id" => 3, "business_filter" => 1),
            array("name" => "Panari Nairobi", "company_id" => 1, "region_id" => 3, "business_filter" => 1),
            array("name" => "Pipeline", "company_id" => 1, "region_id" => 3, "business_filter" => 1),
            array("name" => "Westlands", "company_id" => 1, "region_id" => 3, "business_filter" => 1),

            array("name" => "Buruburu", "company_id" => 1, "region_id" => 4, "business_filter" => 1),
            array("name" => "Eldama Ravine", "company_id" => 1, "region_id" => 4, "business_filter" => 1),
            array("name" => "Githurai", "company_id" => 1, "region_id" => 4, "business_filter" => 1),
            array("name" => "Juja Mall  MediCross", "company_id" => 1, "region_id" => 4, "business_filter" => 1),
            array("name" => "Kiambu", "company_id" => 1, "region_id" => 4, "business_filter" => 1),
            array("name" => "Naivasha", "company_id" => 1, "region_id" => 4, "business_filter" => 1),
            array("name" => "Nakuru", "company_id" => 1, "region_id" => 4, "business_filter" => 1),
            array("name" => "Nakuru  MediCross", "company_id" => 1, "region_id" => 4, "business_filter" => 1),
            array("name" => "Pangani", "company_id" => 1, "region_id" => 4, "business_filter" => 1),
            array("name" => "South C", "company_id" => 1, "region_id" => 4, "business_filter" => 1),
            array("name" => "Thika", "company_id" => 1, "region_id" => 4, "business_filter" => 1),
            array("name" => "Thika MediCross", "company_id" => 1, "region_id" => 4, "business_filter" => 1),

            array("name" => "College House", "company_id" => 1, "region_id" => 5, "business_filter" => 1),
            array("name" => "Daystar Athi River", "company_id" => 1, "region_id" => 5, "business_filter" => 1),
            array("name" => "Daystar Valley Road", "company_id" => 1, "region_id" => 5, "business_filter" => 1),
            array("name" => "Enterprise", "company_id" => 1, "region_id" => 5, "business_filter" => 1),
            array("name" => "Haile Selassie", "company_id" => 1, "region_id" => 5, "business_filter" => 1),
            array("name" => "Machakos", "company_id" => 1, "region_id" => 5, "business_filter" => 1),
            array("name" => "Narok  MediCross", "company_id" => 1, "region_id" => 5, "business_filter" => 1),
            array("name" => "Stanbank Moi Avenue", "company_id" => 1, "region_id" => 5, "business_filter" => 1),
            array("name" => "Teleposta", "company_id" => 1, "region_id" => 5, "business_filter" => 1),
            array("name" => "Umoja", "company_id" => 1, "region_id" => 5, "business_filter" => 1),
            array("name" => "Wote", "company_id" => 1, "region_id" => 5, "business_filter" => 1),
            array("name" => "Windsor Park", "company_id" => 1, "region_id" => 5, "business_filter" => 1),

            array("name" => "Ahero MediCross", "company_id" => 1, "region_id" => 6, "business_filter" => 1),
            array("name" => "Awendo", "company_id" => 1, "region_id" => 6, "business_filter" => 1),
            array("name" => "Homabay", "company_id" => 1, "region_id" => 6, "business_filter" => 1),
            array("name" => "Kericho", "company_id" => 1, "region_id" => 6, "business_filter" => 1),
            array("name" => "Kisii 1", "company_id" => 1, "region_id" => 6, "business_filter" => 1),
            array("name" => "Kisii 2", "company_id" => 1, "region_id" => 6, "business_filter" => 1),
            array("name" => "Kisumu 1", "company_id" => 1, "region_id" => 6, "business_filter" => 1),
            array("name" => "Kisumu 2", "company_id" => 1, "region_id" => 6, "business_filter" => 1),
            array("name" => "Mega CityMediCross", "company_id" => 1, "region_id" => 6, "business_filter" => 1),
            array("name" => "Migori", "company_id" => 2, "region_id" => 6, "business_filter" => 1),
            array("name" => "Nyamira", "company_id" => 1, "region_id" => 6, "business_filter" => 1),
            array("name" => "Sondu", "company_id" => 1, "region_id" => 6, "business_filter" => 1),
            array("name" => "Tuffoam MediCross", "company_id" => 1, "region_id" => 6, "business_filter" => 1),

            array("name" => "Eldoret A", "company_id" => 1, "region_id" => 7, "business_filter" => 1),
            array("name" => "Eldoret MediCross", "company_id" => 1, "region_id" => 7, "business_filter" => 1),
            array("name" => "Iten", "company_id" => 1, "region_id" => 7, "business_filter" => 1),
            array("name" => "Kabarnet", "company_id" => 1, "region_id" => 7, "business_filter" => 1),
            array("name" => "Kapenguria", "company_id" => 1, "region_id" => 7, "business_filter" => 1),
            array("name" => "Kapsabet", "company_id" => 1, "region_id" => 7, "business_filter" => 1),
            array("name" => "Kitale", "company_id" => 1, "region_id" => 7, "business_filter" => 1),
            array("name" => "Maralal", "company_id" => 1, "region_id" => 7, "business_filter" => 1),
            array("name" => "Ol’kalau", "company_id" => 1, "region_id" => 7, "business_filter" => 1),
            array("name" => "Panari Nyahururu", "company_id" => 1, "region_id" => 7, "business_filter" => 1),
            array("name" => "Zion Mall", "company_id" => 1, "region_id" => 7, "business_filter" => 1),

            array("name" => "Bondo", "company_id" => 1, "region_id" => 8, "business_filter" => 1),
            array("name" => "Busia", "company_id" => 1, "region_id" => 8, "business_filter" => 1),
            array("name" => "Kakamega", "company_id" => 1, "region_id" => 8, "business_filter" => 1),
            array("name" => "Kakuma", "company_id" => 1, "region_id" => 8, "business_filter" => 1),
            array("name" => "Kimilili", "company_id" => 1, "region_id" => 8, "business_filter" => 1),
            array("name" => "Malaba", "company_id" => 1, "region_id" => 8, "business_filter" => 1),
            array("name" => "Mbale", "company_id" => 1, "region_id" => 8, "business_filter" => 1),
            array("name" => "Mumias MediCross", "company_id" => 1, "region_id" => 8, "business_filter" => 1),
            array("name" => "Siaya", "company_id" => 1, "region_id" => 8, "business_filter" => 1),
            array("name" => "Webuye", "company_id" => 1, "region_id" => 8, "business_filter" => 1),
            array("name" => "Bungoma", "company_id" => 2, "region_id" => 8, "business_filter" => 1),


        );
        foreach ($facilities as $facility) {
            Facility::updateOrCreate($facility);
        }

        //Seed services
        $services = array(
            array("id" => 1, "name" => "OPTICAL"),
            array("id" => 2, "name" => "PHARMACY"),
            array("id" => 3, "name" => "RADIOLOGY"),
            array("id" => 4, "name" => "LAB"),
            array("id" => 5, "name" => "MEDICAL"),
            array("id" => 6, "name" => "ACCOUNTS"),
            array("id" => 7, "name" => "HR"),
            array("id" => 8, "name" => "CUSTOMER SERVICE"),
            array("id" => 9, "name" => "MEDICAL CENTRE ONE-STOP ISSUES"),
            array("id" => 10, "name" => "ETHICAL"),
            array("id" => 11, "name" => "PREAUTH APPROVAL"),//11
            array("id" => 12, "name" => "MVC REQUESTS"),//12
            array("id" => 13, "name" => "MISSING BUTTON"),//13
            array("id" => 14, "name" => "BILLING"),//14
            array("id" => 15, "name" => "APPOINTMENT"),//15
            array("id" => 16, "name" => "AMBULANCE"),//16
            array("id" => 17, "name" => "UNDERWRITING"),//17
            array("id" => 18, "name" => "UPDATES"),//18
            array("id" => 19, "name" => "COMMUNICATION"),//19
            array("id" => 20, "name" => "QUALITY"),//20
            array("id" => 21, "name" => "TIMINGS"),//21
            array("id" => 22, "name" => "SCHEMES"),//22
            array("id" => 23, "name" => "DISCONNECTED CALL"),//23
            array("id" => 24, "name" => "DOWNTIMES"),//24
            array("id" => 25, "name" => "ACTIVATIONS"),//25
            array("id" => 26, "name" => "ONE TIME PIN"),//26
            array("id" => 27, "name" => "MVC GENERATION"),//27
            array("id" => 28, "name" => "CONSULTATION"),//28
            array("id" => 29, "name" => "INQUIRY"),//29
            array("id" => 30, "name" => "COMPLAIN"),//30
            array("id" => 31, "name" => "COMPLIMENT"),//31
            array("id" => 32, "name" => "TRAINING"),//32

        );

        foreach ($services as $service) {
            Service::updateOrCreate($service);
        }

        //Seed service issues
        $serviceIssues = array(
            //optical
            array("id" => 1, "name" => "Delayed spectacles", "service_id" => 1),
            array("id" => 2, "name" => "Wrong Lenses", "service_id" => 1),
            array("id" => 3, "name" => "No frames", "service_id" => 1),
            array("id" => 4, "name" => "No cases", "service_id" => 1),
            array("id" => 5, "name" => "Lack of Optometrist", "service_id" => 1),

            //Pharmacy
            array("id" => 6, "name" => "Lack of drugs", "service_id" => 2),
            array("id" => 7, "name" => "Incomplete dose", "service_id" => 2),
            array("id" => 8, "name" => "Drug Reaction", "service_id" => 2),
            array("id" => 9, "name" => "Issued wrong drugs", "service_id" => 2),

            //Radiology
            array("id" => 10, "name" => "Lack of the machine", "service_id" => 3),
            array("id" => 11, "name" => "Few staff", "service_id" => 3),
            array("id" => 12, "name" => "image/film not given", "service_id" => 3),

            //Lab
            array("id" => 13, "name" => "Delayed results", "service_id" => 4),
            array("id" => 14, "name" => "Lack of Specimen Holder", "service_id" => 4),
            array("id" => 15, "name" => "WRONG LAB RESULTS", "service_id" => 4),

            ///Medical
            array("id" => 16, "name" => "Wrong Prescription", "service_id" => 5),
            array("id" => 17, "name" => "Wrong Diagnosis", "service_id" => 5),
            array("id" => 18, "name" => "Delayed Appointment", "service_id" => 5),
            array("id" => 19, "name" => "Dental", "service_id" => 5),

            //Acounts
            array("id" => 20, "name" => "Reimbursements on Mpesa situation", "service_id" => 6),
            array("id" => 21, "name" => "Reimbursements on medical situation", "service_id" => 6),
            array("id" => 22, "name" => "No receipt for payment", "service_id" => 6),
            array("id" => 23, "name" => "Payment made to Staff", "service_id" => 6),

            //HR
            array("id" => 24, "name" => "Staff Indiscipline", "service_id" => 7),
            array("id" => 25, "name" => "Bad Attitude by staff", "service_id" => 7),
            array("id" => 26, "name" => "Seeking Employment", "service_id" => 7),

            //Customer Service
            array("id" => 27, "name" => "Staff Grooming", "service_id" => 8),
            array("id" => 28, "name" => "Clinic Hygiene", "service_id" => 8),
            array("id" => 29, "name" => "Look of Clinic", "service_id" => 8),
            array("id" => 30, "name" => "Name Tag", "service_id" => 8),
            array("id" => 31, "name" => "No Uniform", "service_id" => 8),

            //Medical Centre
            array("id" => 32, "name" => "Medical Centre One-Stop issues", "service_id" => 9),

            //Ethical
            array("id" => 33, "name" => "Ethical", "service_id" => 10),

            // preauth service issues
            array("id" => 34, "name" => "Delayed Admission", "service_id" => 11),
            array("id" => 35, "name" => "Delayed Discharge", "service_id" => 11),
            array("id" => 36, "name" => "Rejected Pre Auth", "service_id" => 11),
            array("id" => 37, "name" => "Delayed Email", "service_id" => 11),
            array("id" => 38, "name" => "Less Approval", "service_id" => 11),
            array("id" => 39, "name" => "MVC Opening", "service_id" => 11),
            array("id" => 40, "name" => "Delayed Pre Auth Opening ", "service_id" => 11),
            array("id" => 41, "name" => "Delayed OP Approval", "service_id" => 11),
            array("id" => 42, "name" => "Delayed Dental App", "service_id" => 11),
            array("id" => 43, "name" => "Rejected Dental App", "service_id" => 11),
            array("id" => 44, "name" => "Rejected OP Approval", "service_id" => 11),
            array("id" => 45, "name" => "Referral Services", "service_id" => 11),
            array("id" => 46, "name" => "Incomplete Dosage", "service_id" => 11),
            array("id" => 47, "name" => "Rejected Optical App", "service_id" => 11),
            array("id" => 48, "name" => "Delayed Optical App", "service_id" => 11),
            array("id" => 49, "name" => "Less Dental Approval", "service_id" => 11),
            array("id" => 50, "name" => "Less OP Approval", "service_id" => 11),
            //array("id" => 51, "name" => "Delayed Admission", "service_id" => 11),
            //array("id" => 52, "name" => "Delayed Discharge", "service_id" => 11),
            array("id" => 51, "name" => "Delayed Adm Approval", "service_id" => 11),
            array("id" => 52, "name" => "MVC Generation", "service_id" => 11),
            // MVC service issues
            array("id" => 55, "name" => "MVC Opening", "service_id" => 12),
            array("id" => 56, "name" => "One Time Pin", "service_id" => 12),
            array("id" => 57, "name" => "MVC Generation", "service_id" => 12),

            // Missing button service issues
            array("id" => 58, "name" => "Preauth Button", "service_id" => 13),
            array("id" => 59, "name" => "Claim Button", "service_id" => 13),

            // BILLING service issues
            array("id" => 60, "name" => "Medical Bills", "service_id" => 14),
            array("id" => 61, "name" => "Group Life Cover", "service_id" => 14),
            array("id" => 62, "name" => "Member Benefits", "service_id" => 14),
            array("id" => 63, "name" => "Smart Card Holder", "service_id" => 14),
            array("id" => 64, "name" => "Payment to Staff", "service_id" => 14),
            array("id" => 65, "name" => "Less Dental Approval", "service_id" => 14),
            array("id" => 66, "name" => "Less OP Approval", "service_id" => 14),
            array("id" => 67, "name" => "Co-pay", "service_id" => 14),

            // APPOINTMENTS service issues
            array("id" => 68, "name" => "Facility to Visit", "service_id" => 15),
            array("id" => 69, "name" => "Available Services", "service_id" => 15),

            //AMBULANCE SErvices
            array("id" => 70, "name" => "Ambulance Services", "service_id" => 16),

            // UNDERWRITING service issues
            array("id" => 71, "name" => "Delayed Underwriting", "service_id" => 17),
            array("id" => 72, "name" => "Change of Details", "service_id" => 17),
            array("id" => 73, "name" => "How to Register", "service_id" => 17),
            array("id" => 74, "name" => "Unable to Register", "service_id" => 17),

            // UPDATES service issues
            array("id" => 75, "name" => "TSC Member", "service_id" => 18),
            array("id" => 76, "name" => "NCC Member", "service_id" => 18),
            array("id" => 77, "name" => "Kilifi Member", "service_id" => 18),
            array("id" => 78, "name" => "Panari/MFI Member", "service_id" => 18),
            array("id" => 79, "name" => "KWFT Member", "service_id" => 18),
            array("id" => 80, "name" => "Nyandarwa Member", "service_id" => 18),
            array("id" => 81, "name" => "Narok Member", "service_id" => 18),
            array("id" => 82, "name" => "Busia Member", "service_id" => 18),
            array("id" => 83, "name" => "Mandera Member", "service_id" => 18),
            array("id" => 84, "name" => "Kwale Mamber", "service_id" => 18),
            array("id" => 85, "name" => "Kenya National Library Member", "service_id" => 18),

            // COMMUNICATION service issues
            array("id" => 86, "name" => "Available Services", "service_id" => 19),
            array("id" => 87, "name" => "Medical Reimburse", "service_id" => 19),
            array("id" => 88, "name" => "Mpesa Reimbursement", "service_id" => 19),
            array("id" => 89, "name" => "Delayed Email Response", "service_id" => 19),
            array("id" => 90, "name" => "Seeking SP Payment", "service_id" => 19),
            array("id" => 91, "name" => "Supplier Payment", "service_id" => 19),
            array("id" => 92, "name" => "Seeking Supplier Contract", "service_id" => 19),
            array("id" => 93, "name" => "Seeking SP Contract", "service_id" => 19),
            array("id" => 94, "name" => "Missed Call", "service_id" => 19),
            array("id" => 95, "name" => "Wrong Number", "service_id" => 19),
            array("id" => 96, "name" => "SMS Received", "service_id" => 19),
            array("id" => 97, "name" => "Call by Staff ", "service_id" => 19),
            array("id" => 98, "name" => "Call Transfer ", "service_id" => 19),
            array("id" => 99, "name" => "Last Expense", "service_id" => 19),
            array("id" => 100, "name" => "No Receipt", "service_id" => 19),

            // QUALITY service issues
            array("id" => 101, "name" => "Incomplete Dosage", "service_id" => 20),
            array("id" => 102, "name" => "Lack of Drugs", "service_id" => 20),
            array("id" => 103, "name" => "Lack of Frames", "service_id" => 20),
            array("id" => 104, "name" => "Lack of Optometrist", "service_id" => 20),
            array("id" => 105, "name" => "Lack of Specs Cover", "service_id" => 20),
            array("id" => 106, "name" => "Poor Quality Frames", "service_id" => 20),
            array("id" => 107, "name" => "Wrong Diagnosis ", "service_id" => 20),
            array("id" => 108, "name" => "Drug Reaction", "service_id" => 20),
            array("id" => 109, "name" => "No improvement on treatment", "service_id" => 20),

            // TIMINGS service issues
            array("id" => 110, "name" => "Long Service Time", "service_id" => 21),
            array("id" => 111, "name" => "Delayed Results", "service_id" => 21),
            array("id" => 112, "name" => "Delayed Spectacles", "service_id" => 21),
            array("id" => 113, "name" => "Clinic Operating Hrs", "service_id" => 21),

            // SCHEME service issues
            array("id" => 114, "name" => "TSC Scheme", "service_id" => 22),
            array("id" => 115, "name" => "NCC Scheme", "service_id" => 22),
            array("id" => 116, "name" => "Kilifi Scheme", "service_id" => 22),
            array("id" => 117, "name" => "Narok Scheme", "service_id" => 22),
            array("id" => 118, "name" => "KWFT Scheme", "service_id" => 22),
            array("id" => 119, "name" => "Mandera Scheme", "service_id" => 22),
            array("id" => 120, "name" => "Busia Scheme", "service_id" => 22),
            array("id" => 121, "name" => "Nyandarua Scheme", "service_id" => 22),
            array("id" => 122, "name" => "PANARI/MFI Scheme", "service_id" => 22),
            array("id" => 123, "name" => "Kwale Scheme", "service_id" => 22),
            array("id" => 124, "name" => "Kenya National Library Service", "service_id" => 22),
            array("id" => 125, "name" => "Britam Afya Tele", "service_id" => 22),
            array("id" => 126, "name" => "CIC Afya Bora", "service_id" => 22),
            array("id" => 127, "name" => "Chandarana Food LTD", "service_id" => 22),

            // DISCONNECTED CALL service issues
            array("id" => 128, "name" => "Dropped /Blank  Call", "service_id" => 23),
            array("id" => 129, "name" => "Silent / Mute Calls", "service_id" => 23),
            array("id" => 130, "name" => "Hung Up", "service_id" => 23),

            // DOWNTIMES service issues
            array("id" => 131, "name" => "BMD Not Working", "service_id" => 24),
            array("id" => 132, "name" => "Mediclaim Downtime", "service_id" => 24),
            array("id" => 133, "name" => "Inability to attach", "service_id" => 24),
            array("id" => 134, "name" => "Slow Mediclaim", "service_id" => 24),


            // ACTIVATIONS service issues
            array("id" => 135, "name" => "Medihealth Related", "service_id" => 25),
            array("id" => 136, "name" => "Finger mismatch", "service_id" => 25),
            array("id" => 137, "name" => "Re-enrollment", "service_id" => 25),
            array("id" => 138, "name" => "Activate Department", "service_id" => 25),
            array("id" => 139, "name" => "Scheme Activation", "service_id" => 25),
            array("id" => 140, "name" => "Member Limit Update", "service_id" => 25),

            //ONE TIME PIN SERVICE ISSUES
            array("id" => 141, "name" => "Inpatient Services", "service_id" => 26),
            array("id" => 142, "name" => "Outpatient Service", "service_id" => 26),
            //MVC GENERATION SERVICE ISSUES
            array("id" => 143, "name" => "Inpatient Services", "service_id" => 27),
            array("id" => 144, "name" => "Outpatient Service", "service_id" => 27),
            //CONSULTATION SERVICE ISSUES
            array("id" => 145, "name" => "Chronic Cases", "service_id" => 28),
            array("id" => 146, "name" => "Acute Cases", "service_id" => 28),

            // INQUIRY service issues
            array("id" => 147, "name" => "Nursing Care", "service_id" => 29),
            array("id" => 148, "name" => "Privilege Card", "service_id" => 29),
            array("id" => 149, "name" => "Executive Care", "service_id" => 29),
            array("id" => 150, "name" => "COVID19 Package", "service_id" => 29),
            array("id" => 151, "name" => "Baby Wellness Program", "service_id" => 29),
            array("id" => 152, "name" => "Occupational Health Clinic", "service_id" => 29),
            array("id" => 153, "name" => "Pre Employment Test", "service_id" => 29),

            // COMPLAIN service issues
            array("id" => 154, "name" => "Nursing Care", "service_id" => 30),
            array("id" => 155, "name" => "Privilege Card", "service_id" => 30),
            array("id" => 156, "name" => "Executive Care", "service_id" => 30),
            array("id" => 157, "name" => "COVID19 Package", "service_id" => 30),
            array("id" => 158, "name" => "Baby Wellness Program", "service_id" => 30),
            array("id" => 159, "name" => "Occupational Health Clinic", "service_id" => 30),
            array("id" => 160, "name" => "Pre Employment Test", "service_id" => 30),

            // COMPLIMENT service issues
            array("id" => 161, "name" => "Nursing Care", "service_id" => 31),
            array("id" => 162, "name" => "Privilege Card", "service_id" => 31),
            array("id" => 163, "name" => "Executive Care", "service_id" => 31),
            array("id" => 164, "name" => "COVID19 Package", "service_id" => 31),
            array("id" => 165, "name" => "Baby Wellness Program", "service_id" => 31),
            array("id" => 166, "name" => "Occupational Health Clinic", "service_id" => 31),
            array("id" => 167, "name" => "Pre Employment Test", "service_id" => 31),
            // TRAINING service issues
            array("id" => 168, "name" => "Mediclaim Training", "service_id" => 32),
        );

        foreach ($serviceIssues as $serviceIssue) {
            ServiceIssue::updateOrCreate($serviceIssue);
        }

        //Seed channels
        $reportChannels = array(
            array('name' => 'Email'),
            array('name' => 'Inbound Call'),
            array('name' => 'Outbound Call'),
            array('name' => 'SMS'),
            array('name' => 'Facebook'),
            array('name' => 'Twitter'),
            array('name' => 'Instagram'),
            array('name' => 'Linkedin'),
            array('name' => 'WhatsApp'),
        );
        

        foreach ($reportChannels as $reportChannel) {
            ReportChannel::updateOrCreate($reportChannel);
        }

        $companies = array(
            array("id" => 1, 'name' => 'Bliss HealthCare'),
            array("id" => 2, 'name' => 'LifeCare Hospitals'),
            array("id" => 3, 'name' => 'Others'), 
        );

        foreach ($companies as $company) {
            Company::updateOrCreate($company);
        }

        //Seed users
        $str = 'medbookafrica';
        $password = \Hash::make($str);
        $users = array(
            array(
                array('email' => "ms.central@blisshealthcare.co.ke"),
                array(
                    'first_name' => 'Kennedy',
                    'last_name' => 'Maru',
                    'user_name' => 'ms.central',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                    'region_id' => 1,
                ),
            ),
            array(
                array('email' => "ms.coast@blisshealthcare.co.ke"),
                array(
                    'first_name' => 'Benson',
                    'last_name' => 'Chisaga',
                    'user_name' => 'ms.coast',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                    'region_id' => 2,
                ),
            ),
            array(
                array('email' => "ms.nairobicentral@blisshealthcare.co.ke"),
                array(
                    'first_name' => 'Noel',
                    'last_name' => 'Mutinda',
                    'user_name' => 'ms.nairobicentral',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                    'region_id' => 3,
                ),
            ),
            array(
                array('email' => "ms.nairobinorth@blisshealthcare.co.ke"),
                array(
                    'first_name' => 'Alex',
                    'last_name' => 'Muraguri',
                    'user_name' => 'ms.nairobinorth',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                    'region_id' => 4,
                ),
            ),
            array(
                array('email' => "ms.nairobisouth@blisshealthcare.co.ke"),
                array(
                    'first_name' => 'Batuli',
                    'last_name' => 'Mzee',
                    'user_name' => 'ms.nairobisouth',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                    'region_id' => 5,
                ),
            ),
            array(
                array('email' => "ms.nyanza@blisshealthcare.co.ke"),
                array(
                    'first_name' => 'Kevin',
                    'last_name' => 'Moracha',
                    'user_name' => 'ms.nyanza',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                    'region_id' => 6,
                ),
            ),
            array(
                array('email' => "ms.riftvalley@blisshealthcare.co.ke"),
                array(
                    'first_name' => 'Robert',
                    'last_name' => 'Sigei',
                    'user_name' => 'ms.riftvalley',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                    'region_id' => 7,
                ),
            ),
            array(
                array('email' => "ms.western@blisshealthcare.co.ke"),
                array(
                    'first_name' => 'Austin',
                    'last_name' => 'Okoth',
                    'user_name' => 'ms.western',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                    'region_id' => 8,
                ),
            ),
            array(
                array('email' => "hod.optical@blisshealthcare.co.ke"),
                array(
                    'first_name' => 'Dennis',
                    'last_name' => 'Kiprop',
                    'user_name' => 'hod.optical',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                ),
            ),
            array(
                array('email' => "lab.manager@blisshealthcare.co.ke"),
                array(
                    'first_name' => 'Andrew',
                    'last_name' => 'Chege',
                    'user_name' => 'lab.manager',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                ),
            ),
            array(
                array('email' => "pharmacy@blisshealthcare.co.ke"),
                array(
                    'first_name' => 'Stephen',
                    'last_name' => 'Chege',
                    'user_name' => 'pharmacy',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                ),
            ),
            array(
                array('email' => "radiology@blisshealthcare.co.ke"),
                array(
                    'first_name' => 'Felix',
                    'last_name' => 'Omune',
                    'user_name' => 'radiology',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                ),
            ),
            array(
                array('email' => "selepe.percival@blisshealthcare.co.ke"),
                array(
                    'first_name' => 'Selepe',
                    'last_name' => 'Percival',
                    'user_name' => 'selepe.percival',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                ),
            ),
            array(
                array('email' => "dental@blisshealthcare.co.ke"),
                array(
                    'first_name' => 'Abraham',
                    'last_name' => 'Marenya',
                    'user_name' => 'dental',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                ),
            ),
            array(
                array('email' => "rm.central@blisshealthcare.co.ke"),
                array(
                    'first_name' => 'Nathan',
                    'last_name' => 'Kiplagat',
                    'user_name' => 'rm.central',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                    'region_id' => 1,
                ),
            ),
            array(
                array('email' => "rm.nairobicentral@blisshealthcare.co.ke"),
                array(
                    'first_name' => 'Heckman',
                    'last_name' => 'Wasuna',
                    'user_name' => 'rm.nairobicentral',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                    'region_id' => 3,
                ),
            ),
            array(
                array('email' => "rm.nairobinorth@blisshealthcare.co.ke"),
                array(
                    'first_name' => 'Naomi',
                    'last_name' => 'Sikuku',
                    'user_name' => 'rm.nairobinorth',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                    'region_id' => 4,
                ),
            ),
            array(
                array('email' => "rm.nairobisouth@blisshealthcare.co.ke"),
                array(
                    'first_name' => 'Monica',
                    'last_name' => 'Kaumo',
                    'user_name' => 'rm.nairobisouth',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                    'region_id' => 5,
                ),
            ),
            array(
                array('email' => "ms.nyanza@blisshealthcare.co.ke"),
                array(
                    'first_name' => 'Joshua',
                    'last_name' => 'Mutanyi',
                    'user_name' => 'ms.nyanza',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                    'region_id' => 6,
                ),
            ),
            array(
                array('email' => "rm.riftvalley@blisshealthcare.co.ke"),
                array(
                    'first_name' => 'Nathan',
                    'last_name' => 'Kiplagat',
                    'user_name' => 'rm.riftvalley',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                    'region_id' => 7,
                ),
            ),
            array(
                array('email' => "rm.western@blisshealthcare.co.ke"),
                array(
                    'first_name' => 'Job',
                    'last_name' => 'Ochieng',
                    'user_name' => 'rm.western',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                    'region_id' => 8,
                ),
            ),
            array(
                array('email' => "finance02@blisshealthcare.co.ke"),
                array(
                    'first_name' => 'Antony',
                    'last_name' => 'Kioma',
                    'user_name' => 'finance02',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                ),
            ),
            array(
                array('email' => "vivek.modi@blisshealthcare.co.ke"),
                array(
                    'first_name' => 'Vivek',
                    'last_name' => 'Modi',
                    'user_name' => 'vivek.modi',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                ),
            ),
            array(
                array('email' => "hr.manager@blisshealthcare.co.ke"),
                array(
                    'first_name' => 'Cathrine',
                    'last_name' => 'Mungiru',
                    'user_name' => 'hr.manager',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                ),
            ),
            array(
                array('email' => "hr@blisshealthcare.co.ke"),
                array(
                    'first_name' => 'Paul',
                    'last_name' => 'Mucheru',
                    'user_name' => 'hr',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                ),
            ),
            array(
                array('email' => "Lorraine.Wambita@BLISSHEALTHCARE.CO.KE"),
                array(
                    'first_name' => 'Lorraine',
                    'last_name' => 'Wambita',
                    'user_name' => 'lorraine.wambita',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                ),
            ),
            array(
                array('email' => "Vishal.Sharma@BLISSHEALTHCARE.CO.KE"),
                array(
                    'first_name' => 'Vishal',
                    'last_name' => 'Sharma',
                    'user_name' => 'vishal.sharma ',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                ),
            ),

            // MAKL USERS
            array(
                array('email' => "medical@makl.co.ke"),
                array(
                    'first_name' => 'Medical',
                    'last_name' => 'Team',
                    'user_name' => 'medical',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                ),
            ),
            array(
                array('email' => "support@makl.co.ke"),
                array(
                    'first_name' => 'Support',
                    'last_name' => 'Team',
                    'user_name' => 'support',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                ),
            ),
            array(
                array('email' => "service.excellence@makl.co.ke"),
                array(
                    'first_name' => 'Service',
                    'last_name' => 'Excellence',
                    'user_name' => 'service.excellence',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                ),
            ),
            array(
                array('email' => "accounts.recon@makl.co.ke"),
                array(
                    'first_name' => 'Catherine',
                    'last_name' => 'Moraa',
                    'user_name' => 'accounts.recon',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                ),
            ),
            array(
                array('email' => "vivek.modi@blissgvs.co.ke"),
                array(
                    'first_name' => 'Vivek',
                    'last_name' => 'modi',
                    'user_name' => 'vivek.modi2',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                ),
            ),
            array(
                array('email' => "gary.wandiga@blissgvs.co.ke"),
                array(
                    'first_name' => 'Gary',
                    'last_name' => 'Wandiga',
                    'user_name' => 'garry.wandiga',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                ),
            ),
            array(
                array('email' => "john.mark@makl.co.ke"),
                array(
                    'first_name' => 'John',
                    'last_name' => 'Mark',
                    'user_name' => 'john.mark',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                ),
            ),
            array(
                array('email' => "ncc@makl.co.ke"),
                array(
                    'first_name' => 'Elian',
                    'last_name' => 'Njuguna',
                    'user_name' => 'ncc',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                ),
            ),
            array(
                array('email' => "Sm.kilifi@makl.co.ke"),
                array(
                    'first_name' => 'Rhoda',
                    'last_name' => 'Muganda',
                    'user_name' => 'Sm.kilifi',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                ),
            ),
            array(
                array('email' => "IT@makl.co.ke"),
                array(
                    'first_name' => 'IT',
                    'last_name' => 'Team',
                    'user_name' => 'IT.makl',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                ),
            ),
            array(
                array('email' => "training@makl.co.ke"),
                array(
                    'first_name' => 'Training',
                    'last_name' => 'Team',
                    'user_name' => 'training',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                ),
            ),
            array(
                array('email' => "carolyne.kariuki@blisshealthcare.co.ke"),
                array(
                    'first_name' => 'Tele medicine',
                    'last_name' => 'Team',
                    'user_name' => 'carolyne.kariuki',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                ),
            ),
            array(
                array('email' => "Mrityunjay.Singh@makl.co.ke"),
                array(
                    'first_name' => 'Mrityunjay',
                    'last_name' => 'Singh',
                    'user_name' => 'mrityunjay.singh',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                ),
            ),
            array(
                array('email' => "gurumeet.Kumar@makl.coke"),
                array(
                    'first_name' => 'Gurumeet',
                    'last_name' => 'Kumar',
                    'user_name' => 'gurumeet.kumar',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                ),
            ),
            array(
                array('email' => "Outbound@makl.co.ke"),
                array(
                    'first_name' => 'Cyrus',
                    'last_name' => 'Choge',
                    'user_name' => 'Outbound',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                ),
            ),
            array(
                array('email' => "sheila.joy@makl.co.ke"),
                array(
                    'first_name' => 'Sheila',
                    'last_name' => 'Joy',
                    'user_name' => 'sheila.joy',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                ),
            ),
            array(
                array('email' => "Pradeep.Meharishi@makl.co.ke"),
                array(
                    'first_name' => 'Pradeep',
                    'last_name' => 'Meharishi',
                    'user_name' => 'pradeep.meharishi',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                ),
            ),
            array(
                array('email' => "rishi.pincha@nmcafrica.com"),
                array(
                    'first_name' => 'Rishi',
                    'last_name' => 'Pincha',
                    'user_name' => 'rishi.pincha',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                ),
            ),
            array(
                array('email' => "Kevin.Muraba@BLISSHEALTHCARE.CO.KE"),
                array(
                    'first_name' => 'Kevin',
                    'last_name' => 'Muraba',
                    'user_name' => 'kevin.muraba',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                ),
            ),
            array(
                array('email' => "Agnes.kariuki@makl.co.ke"),
                array(
                    'first_name' => 'Agnes',
                    'last_name' => 'Kariuki',
                    'user_name' => 'agnes.kariuki',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                ),
            ),
            array(
                array('email' => "ali@makl.co.ke"),
                array(
                    'first_name' => 'Ali',
                    'last_name' => 'Asif',
                    'user_name' => 'ali.asif',
                    'password' => $password,
                    'assign_group' => 1,
                    'primary_dpt' => 1,
                    'active' => 1,
                    'role' => 'agent',
                ),
            ),
        );

        foreach ($users as $user) {
            User::updateOrCreate($user[0], $user[1]);
        }

        //Seed user escalations
        $userEscalations = array();
        for ($i = 1; $i <= 19; $i++) {
            array_push($userEscalations, array(
                "user" => "ms.central@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 1, "service_issue_id" => $i, "escalation_level" => 1,
            ));
        }
        for ($i = 1; $i <= 19; $i++) {
            array_push($userEscalations, array(
                "user" => "ms.coast@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 2, "service_issue_id" => $i, "escalation_level" => 1,
            ));
        }
        for ($i = 1; $i <= 19; $i++) {
            array_push($userEscalations, array(
                "user" => "ms.nairobicentral@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 3, "service_issue_id" => $i, "escalation_level" => 1,
            ));
        }
        for ($i = 1; $i <= 19; $i++) {
            array_push($userEscalations, array(
                "user" => "ms.nairobinorth@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 4, "service_issue_id" => $i, "escalation_level" => 1,
            ));
        }
        for ($i = 1; $i <= 19; $i++) {
            array_push($userEscalations, array(
                "user" => "ms.nairobisouth@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 5, "service_issue_id" => $i, "escalation_level" => 1,
            ));
        }
        for ($i = 1; $i <= 19; $i++) {
            array_push($userEscalations, array(
                "user" => "ms.nyanza@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 6, "service_issue_id" => $i, "escalation_level" => 1,
            ));
        }
        for ($i = 1; $i <= 19; $i++) {
            array_push($userEscalations, array(
                "user" => "ms.riftvalley@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 7, "service_issue_id" => $i, "escalation_level" => 1,
            ));
        }
        for ($i = 1; $i <= 19; $i++) {
            array_push($userEscalations, array(
                "user" => "ms.western@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 8, "service_issue_id" => $i, "escalation_level" => 1,
            ));
        }


        for ($i = 1; $i <= 8; $i++) {
            for ($j = 1; $j <= 5; $j++) {
                array_push($userEscalations, array(
                    "user" => "hod.optical@blisshealthcare.co.ke", "company_id" => 1, "region_id" => $i, "service_issue_id" => $j, "escalation_level" => 2,
                ));
            }
        }
        for ($i = 1; $i <= 8; $i++) {
            for ($j = 6; $j <= 9; $j++) {
                array_push($userEscalations, array(
                    "user" => "pharmacy@blisshealthcare.co.ke", "company_id" => 1, "region_id" => $i, "service_issue_id" => $j, "escalation_level" => 2,
                ));
            }
        }
        for ($i = 1; $i <= 8; $i++) {
            for ($j = 10; $j <= 11; $j++) {
                array_push($userEscalations, array(
                    "user" => "radiology@blisshealthcare.co.ke", "company_id" => 1, "region_id" => $i, "service_issue_id" => $j, "escalation_level" => 2,
                ));
            }
        }
        for ($i = 1; $i <= 8; $i++) {
            for ($j = 13; $j <= 15; $j++) {
                array_push($userEscalations, array(
                    "user" => "lab.manager@blisshealthcare.co.ke", "company_id" => 1, "region_id" => $i, "service_issue_id" => $j, "escalation_level" => 2,
                ));
            }
        }
        for ($i = 1; $i <= 8; $i++) {
            for ($j = 16; $j <= 18; $j++) {
                array_push($userEscalations, array(
                    "user" => "specialist.clinics@blisshealthcare.co.ke", "company_id" => 1, "region_id" => $i, "service_issue_id" => $j, "escalation_level" => 2,
                ));
            }
        }

        for ($i = 1; $i <= 8; $i++) {
            array_push($userEscalations, array(
                "user" => "dental@blisshealthcare.co.ke", "company_id" => 1, "region_id" => $i, "service_issue_id" => 19, "escalation_level" => 2,
            ));
        }


        for ($i = 1; $i <= 8; $i++) {
            for ($j = 1; $j <= 5; $j++) {
                array_push($userEscalations, array(
                    "user" => "lab.manager@blisshealthcare.co.ke", "company_id" => 1, "region_id" => $i, "service_issue_id" => $j, "escalation_level" => 3,
                ));
            }
        }
        for ($i = 1; $i <= 8; $i++) {
            for ($j = 6; $j <= 9; $j++) {
                array_push($userEscalations, array(
                    "user" => "specialist.clinics@blisshealthcare.co.ke", "company_id" => 1, "region_id" => $i, "service_issue_id" => $j, "escalation_level" => 3,
                ));
            }
        }
        for ($i = 1; $i <= 8; $i++) {
            for ($j = 10; $j <= 11; $j++) {
                array_push($userEscalations, array(
                    "user" => "lab.manager@blisshealthcare.co.ke", "company_id" => 1, "region_id" => $i, "service_issue_id" => $j, "escalation_level" => 3,
                ));
            }
        }
        for ($i = 1; $i <= 8; $i++) {
            for ($j = 13; $j <= 15; $j++) {
                array_push($userEscalations, array(
                    "user" => "selepe.percival@blisshealthcare.co.ke", "company_id" => 1, "region_id" => $i, "service_issue_id" => $j, "escalation_level" => 3,
                ));
            }
        }
        for ($i = 1; $i <= 8; $i++) {
            for ($j = 16; $j <= 18; $j++) {
                array_push($userEscalations, array(
                    "user" => "selepe.percival@blisshealthcare.co.ke", "company_id" => 1, "region_id" => $i, "service_issue_id" => $j, "escalation_level" => 3,
                ));
            }
        }

        for ($i = 1; $i <= 8; $i++) {
            array_push($userEscalations, array(
                "user" => "lab.manager@blisshealthcare.co.ke", "company_id" => 1, "region_id" => $i, "service_issue_id" => 19, "escalation_level" => 3,
            ));
        }


        //NON BLISS
        for ($i = 20; $i <= 29; $i++) {
            array_push($userEscalations, array(
                "user" => "rm.central@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 1, "service_issue_id" => $i, "escalation_level" => 1,
            ));
        }
        for ($i = 20; $i <= 29; $i++) {
            array_push($userEscalations, array(
                "user" => "ms.coast@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 2, "service_issue_id" => $i, "escalation_level" => 1,
            ));
        }
        for ($i = 20; $i <= 29; $i++) {
            array_push($userEscalations, array(
                "user" => "rm.nairobicentral@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 3, "service_issue_id" => $i, "escalation_level" => 1,
            ));
        }
        for ($i = 20; $i <= 29; $i++) {
            array_push($userEscalations, array(
                "user" => "rm.nairobinorth@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 4, "service_issue_id" => $i, "escalation_level" => 1,
            ));
        }
        for ($i = 20; $i <= 29; $i++) {
            array_push($userEscalations, array(
                "user" => "rm.nairobisouth@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 5, "service_issue_id" => $i, "escalation_level" => 1,
            ));
        }
        for ($i = 20; $i <= 29; $i++) {
            array_push($userEscalations, array(
                "user" => "rm.nyanza@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 6, "service_issue_id" => $i, "escalation_level" => 1,
            ));
        }
        for ($i = 20; $i <= 29; $i++) {
            array_push($userEscalations, array(
                "user" => "rm.riftvalley@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 7, "service_issue_id" => $i, "escalation_level" => 1,
            ));
        }
        for ($i = 20; $i <= 29; $i++) {
            array_push($userEscalations, array(
                "user" => "rm.western@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 8, "service_issue_id" => $i, "escalation_level" => 1,
            ));
        }

        array_push($userEscalations, array(
            "user" => "hr1@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 7, "service_issue_id" => 30, "escalation_level" => 1,
        ));
        array_push($userEscalations, array(
            "user" => "hr1@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 4, "service_issue_id" => 30, "escalation_level" => 1,
        ));
        array_push($userEscalations, array(
            "user" => "hr4@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 5, "service_issue_id" => 30, "escalation_level" => 1,
        ));
        array_push($userEscalations, array(
            "user" => "hr4@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 2, "service_issue_id" => 30, "escalation_level" => 1,
        ));
        array_push($userEscalations, array(
            "user" => "hr2@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 1, "service_issue_id" => 30, "escalation_level" => 1,
        ));
        array_push($userEscalations, array(
            "user" => "hr2@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 3, "service_issue_id" => 30, "escalation_level" => 1,
        ));
        array_push($userEscalations, array(
            "user" => "hr3@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 6, "service_issue_id" => 30, "escalation_level" => 1,
        ));
        array_push($userEscalations, array(
            "user" => "hr3@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 8, "service_issue_id" => 30, "escalation_level" => 1,
        ));


        array_push($userEscalations, array(
            "user" => "rm.riftvalley@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 7, "service_issue_id" => 31, "escalation_level" => 1,
        ));
        array_push($userEscalations, array(
            "user" => "rm.nairobinorth@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 4, "service_issue_id" => 31, "escalation_level" => 1,
        ));
        array_push($userEscalations, array(
            "user" => "rm.nairobisouth@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 5, "service_issue_id" => 31, "escalation_level" => 1,
        ));
        array_push($userEscalations, array(
            "user" => "ms.coast@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 2, "service_issue_id" => 31, "escalation_level" => 1,
        ));
        array_push($userEscalations, array(
            "user" => "rm.central@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 1, "service_issue_id" => 31, "escalation_level" => 1,
        ));
        array_push($userEscalations, array(
            "user" => "rm.nairobicentral@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 3, "service_issue_id" => 31, "escalation_level" => 1,
        ));
        array_push($userEscalations, array(
            "user" => "rm.nyanza@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 6, "service_issue_id" => 31, "escalation_level" => 1,
        ));
        array_push($userEscalations, array(
            "user" => "rm.western@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 8, "service_issue_id" => 31, "escalation_level" => 1,
        ));


        array_push($userEscalations, array(
            "user" => "ms.riftvalley@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 7, "service_issue_id" => 33, "escalation_level" => 1,
        ));
        array_push($userEscalations, array(
            "user" => "ms.nairobinorth@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 4, "service_issue_id" => 33, "escalation_level" => 1,
        ));
        array_push($userEscalations, array(
            "user" => "ms.nairobisouth@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 5, "service_issue_id" => 33, "escalation_level" => 1,
        ));
        array_push($userEscalations, array(
            "user" => "ms.coast@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 2, "service_issue_id" => 33, "escalation_level" => 1,
        ));
        array_push($userEscalations, array(
            "user" => "ms.central@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 1, "service_issue_id" => 33, "escalation_level" => 1,
        ));
        array_push($userEscalations, array(
            "user" => "ms.nairobicentral@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 3, "service_issue_id" => 33, "escalation_level" => 1,
        ));
        array_push($userEscalations, array(
            "user" => "ms.nyanza@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 6, "service_issue_id" => 33, "escalation_level" => 1,
        ));
        array_push($userEscalations, array(
            "user" => "ms.western@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 8, "service_issue_id" => 33, "escalation_level" => 1,
        ));


        for ($i = 1; $i <= 8; $i++) {
            for ($j = 20; $j <= 23; $j++) {
                array_push($userEscalations, array(
                    "user" => "finance02@blisshealthcare.co.ke", "company_id" => 1, "region_id" => $i, "service_issue_id" => $j, "escalation_level" => 2,
                ));
            }
        }
        for ($i = 1; $i <= 8; $i++) {
            for ($j = 20; $j <= 23; $j++) {
                array_push($userEscalations, array(
                    "user" => "vivek.modi@blisshealthcare.co.ke", "company_id" => 1, "region_id" => $i, "service_issue_id" => $j, "escalation_level" => 3,
                ));
            }
        }


        for ($i = 24; $i <= 27; $i++) {
            array_push($userEscalations, array(
                "user" => "hr.manager@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 7, "service_issue_id" => $i, "escalation_level" => 2,
            ));
            array_push($userEscalations, array(
                "user" => "hr.manager@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 4, "service_issue_id" => $i, "escalation_level" => 2,
            ));
            array_push($userEscalations, array(
                "user" => "hr.manager@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 5, "service_issue_id" => $i, "escalation_level" => 2,
            ));
            array_push($userEscalations, array(
                "user" => "hr.manager@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 2, "service_issue_id" => $i, "escalation_level" => 2,
            ));
            array_push($userEscalations, array(
                "user" => "Lorraine.Wambita@BLISSHEALTHCARE.CO.KE", "company_id" => 1, "region_id" => 1, "service_issue_id" => $i, "escalation_level" => 2,
            ));
            array_push($userEscalations, array(
                "user" => "Lorraine.Wambita@BLISSHEALTHCARE.CO.KE", "company_id" => 1, "region_id" => 3, "service_issue_id" => $i, "escalation_level" => 2,
            ));
            array_push($userEscalations, array(
                "user" => "Lorraine.Wambita@BLISSHEALTHCARE.CO.KE", "company_id" => 1, "region_id" => 6, "service_issue_id" => $i, "escalation_level" => 2,
            ));
            array_push($userEscalations, array(
                "user" => "Lorraine.Wambita@BLISSHEALTHCARE.CO.KE", "company_id" => 1, "region_id" => 8, "service_issue_id" => $i, "escalation_level" => 2,
            ));
        }

        for ($i = 24; $i <= 27; $i++) {
            for ($j = 1; $j <= 8; $j++) {
                array_push($userEscalations, array(
                    "user" => "hr@blisshealthcare.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 3,
                ));
            }
        }
        for ($i = 28; $i <= 29; $i++) {
            for ($j = 1; $j <= 8; $j++) {
                array_push($userEscalations, array(
                    "user" => "Vishal.Sharma@BLISSHEALTHCARE.CO.KE", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 2,
                ));
            }
        }

        for ($i = 30; $i <= 31; $i++) {
            array_push($userEscalations, array(
                "user" => "hr.manager@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 7, "service_issue_id" => $i, "escalation_level" => 2,
            ));
            array_push($userEscalations, array(
                "user" => "hr.manager@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 4, "service_issue_id" => $i, "escalation_level" => 2,
            ));
            array_push($userEscalations, array(
                "user" => "hr.manager@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 5, "service_issue_id" => $i, "escalation_level" => 2,
            ));
            array_push($userEscalations, array(
                "user" => "hr.manager@blisshealthcare.co.ke", "company_id" => 1, "region_id" => 2, "service_issue_id" => $i, "escalation_level" => 2,
            ));
            array_push($userEscalations, array(
                "user" => "Lorraine.Wambita@BLISSHEALTHCARE.CO.KE", "company_id" => 1, "region_id" => 1, "service_issue_id" => $i, "escalation_level" => 2,
            ));
            array_push($userEscalations, array(
                "user" => "Lorraine.Wambita@BLISSHEALTHCARE.CO.KE", "company_id" => 1, "region_id" => 3, "service_issue_id" => $i, "escalation_level" => 2,
            ));
            array_push($userEscalations, array(
                "user" => "Lorraine.Wambita@BLISSHEALTHCARE.CO.KE", "company_id" => 1, "region_id" => 6, "service_issue_id" => $i, "escalation_level" => 2,
            ));
            array_push($userEscalations, array(
                "user" => "Lorraine.Wambita@BLISSHEALTHCARE.CO.KE", "company_id" => 1, "region_id" => 8, "service_issue_id" => $i, "escalation_level" => 2,
            ));
        }

        for ($j = 1; $j <= 8; $j++) {
            array_push($userEscalations, array(
                "user" => "specialist.clinics@blisshealthcare.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => 33, "escalation_level" => 2,
            ));
        }

        for ($j = 1; $j <= 8; $j++) {
            array_push($userEscalations, array(
                "user" => "selepe.percival@blisshealthcare.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => 33, "escalation_level" => 3,
            ));
        }

        //MAKL ESCALATIONS
        for ($j = 1; $j <= 8; $j++) {

            //level 1
            for ($i = 34; $i <= 52; $i++) {
                array_push($userEscalations, array(
                    "user" => "medical@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 1,
                ));
            }
            array_push($userEscalations, array(
                "user" => "medical@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => 55, "escalation_level" => 1,
            ));
            for ($i = 58; $i <= 59; $i++) {
                array_push($userEscalations, array(
                    "user" => "support@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 1,
                ));
            }
            array_push($userEscalations, array(
                "user" => "service.excellence@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => 64, "escalation_level" => 1,
            ));
            for ($i = 55; $i <= 59; $i++) {
                array_push($userEscalations, array(
                    "user" => "medical@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 1,
                ));
            }
            for ($i = 65; $i <= 66; $i++) {
                array_push($userEscalations, array(
                    "user" => "medical@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 1,
                ));
            }
            array_push($userEscalations, array(
                "user" => "medical@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => 70, "escalation_level" => 1,
            ));
            for ($i = 71; $i <= 72; $i++) {
                array_push($userEscalations, array(
                    "user" => "support@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 1,
                ));
            }
            for ($i = 74; $i <= 85; $i++) {
                array_push($userEscalations, array(
                    "user" => "support@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 1,
                ));
            }
            for ($i = 86; $i <= 89; $i++) {
                array_push($userEscalations, array(
                    "user" => "service.excellence@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 1,
                ));
            }
            array_push($userEscalations, array(
                "user" => "accounts.recon@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => 90, "escalation_level" => 1,
            ));
            array_push($userEscalations, array(
                "user" => "vivek.modi@blissgvs.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => 91, "escalation_level" => 1,
            ));
            array_push($userEscalations, array(
                "user" => "gary.wandiga@blissgvs.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => 92, "escalation_level" => 1,
            ));
            array_push($userEscalations, array(
                "user" => "john.mark@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => 93, "escalation_level" => 1,
            ));
            array_push($userEscalations, array(
                "user" => "cs@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => 99, "escalation_level" => 1,
            ));
            for ($i = 100; $i <= 113; $i++) {
                array_push($userEscalations, array(
                    "user" => "service.excellence@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 1,
                ));
            }
            for ($i = 25; $i <= 26; $i++) {
                array_push($userEscalations, array(
                    "user" => "service.excellence@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 1,
                ));
            }
            array_push($userEscalations, array(
                "user" => "john.mark@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => 114, "escalation_level" => 1,
            ));
            array_push($userEscalations, array(
                "user" => "ncc@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => 115, "escalation_level" => 1,
            ));
            for ($i = 116; $i <= 127; $i++) {
                array_push($userEscalations, array(
                    "user" => "Sm.kilifi@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 1,
                ));
            }
            for ($i = 128; $i <= 129; $i++) {
                array_push($userEscalations, array(
                    "user" => "IT@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 1,
                ));
            }
            array_push($userEscalations, array(
                "user" => "training@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => 168, "escalation_level" => 1,
            ));
            for ($i = 131; $i <= 144; $i++) {
                array_push($userEscalations, array(
                    "user" => "support@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 1,
                ));
            }
            for ($i = 145; $i <= 146; $i++) {
                array_push($userEscalations, array(
                    "user" => "carolyne.kariuki@blisshealthcare.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 1,
                ));
            }
            for ($i = 154; $i <= 167; $i++) {
                array_push($userEscalations, array(
                    "user" => "carolyne.kariuki@blisshealthcare.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 1,
                ));
            }


            //Level 2
            for ($i = 34; $i <= 52; $i++) {
                array_push($userEscalations, array(
                    "user" => "Mrityunjay.Singh@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 2,
                ));
            }
            array_push($userEscalations, array(
                "user" => "gurumeet.Kumar@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => 55, "escalation_level" => 2,
            ));
            for ($i = 56; $i <= 59; $i++) {
                array_push($userEscalations, array(
                    "user" => "Outbound@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 2,
                ));
            }

            array_push($userEscalations, array(
                "user" => "sheila.joy@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => 64, "escalation_level" => 2,
            ));

            for ($i = 65; $i <= 66; $i++) {
                array_push($userEscalations, array(
                    "user" => "medical@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 2,
                ));
            }
            array_push($userEscalations, array(
                "user" => "Mrityunjay.Singh@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => 70, "escalation_level" => 2,
            ));
            for ($i = 71; $i <= 72; $i++) {
                array_push($userEscalations, array(
                    "user" => "Outbound@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 2,
                ));
            }
            for ($i = 74; $i <= 85; $i++) {
                array_push($userEscalations, array(
                    "user" => "Outbound@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 2,
                ));
            }
            for ($i = 86; $i <= 89; $i++) {
                array_push($userEscalations, array(
                    "user" => "sheila.joy@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 2,
                ));
            }
            array_push($userEscalations, array(
                "user" => "Pradeep.Meharishi@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => 90, "escalation_level" => 2,
            ));
            array_push($userEscalations, array(
                "user" => "rishi.pincha@nmcafrica.com", "company_id" => 1, "region_id" => $j, "service_issue_id" => 92, "escalation_level" => 2,
            ));

            array_push($userEscalations, array(
                "user" => "sheila.joy@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => 99, "escalation_level" => 2,
            ));
            for ($i = 100; $i <= 113; $i++) {
                array_push($userEscalations, array(
                    "user" => "sheila.joy@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 2,
                ));
            }
            for ($i = 25; $i <= 26; $i++) {
                array_push($userEscalations, array(
                    "user" => "sheila.joy@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 2,
                ));
            }
            array_push($userEscalations, array(
                "user" => "sheila.joy@makl.co.kee", "company_id" => 1, "region_id" => $j, "service_issue_id" => 114, "escalation_level" => 2,
            ));
            array_push($userEscalations, array(
                "user" => "sheila.joy@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => 115, "escalation_level" => 2,
            ));
            for ($i = 116; $i <= 127; $i++) {
                array_push($userEscalations, array(
                    "user" => "sheila.joy@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 2,
                ));
            }
            for ($i = 128; $i <= 129; $i++) {
                array_push($userEscalations, array(
                    "user" => "Kevin.Muraba@BLISSHEALTHCARE.CO.KE", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 2,
                ));
            }
            array_push($userEscalations, array(
                "user" => "Agnes.kariuki@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => 168, "escalation_level" => 2,
            ));
            for ($i = 131; $i <= 144; $i++) {
                array_push($userEscalations, array(
                    "user" => "Outbound@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 2,
                ));
            }
            for ($i = 145; $i <= 146; $i++) {
                array_push($userEscalations, array(
                    "user" => "sheila.joy@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 2,
                ));
            }
            for ($i = 154; $i <= 167; $i++) {
                array_push($userEscalations, array(
                    "user" => "sheila.joy@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 2,
                ));
            }

            //Level 3
            for ($i = 34; $i <= 52; $i++) {
                array_push($userEscalations, array(
                    "user" => "Pradeep.Meharishi@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 3,
                ));
            }
            array_push($userEscalations, array(
                "user" => "Pradeep.Meharishi@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => 55, "escalation_level" => 3,
            ));
            for ($i = 56; $i <= 59; $i++) {
                array_push($userEscalations, array(
                    "user" => "sheila.joy@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 3,
                ));
            }

            for ($i = 65; $i <= 66; $i++) {
                array_push($userEscalations, array(
                    "user" => "Pradeep.Meharishi@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 3,
                ));
            }
            array_push($userEscalations, array(
                "user" => "Pradeep.Meharishi@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => 70, "escalation_level" => 3,
            ));
            for ($i = 71; $i <= 72; $i++) {
                array_push($userEscalations, array(
                    "user" => "sheila.joy@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 3,
                ));
            }
            for ($i = 74; $i <= 85; $i++) {
                array_push($userEscalations, array(
                    "user" => "sheila.joy@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 3,
                ));
            }

            array_push($userEscalations, array(
                "user" => "ali@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => 90, "escalation_level" => 3,
            ));

            for ($i = 128; $i <= 129; $i++) {
                array_push($userEscalations, array(
                    "user" => "sheila.joy@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 3,
                ));
            }
            array_push($userEscalations, array(
                "user" => "ali@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => 168, "escalation_level" => 3,
            ));
            for ($i = 131; $i <= 144; $i++) {
                array_push($userEscalations, array(
                    "user" => "sheila.joy@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 3,
                ));
            }
            for ($i = 145; $i <= 146; $i++) {
                array_push($userEscalations, array(
                    "user" => "Ali@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 3,
                ));
            }
            for ($i = 154; $i <= 167; $i++) {
                array_push($userEscalations, array(
                    "user" => "Ali@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 3,
                ));
            }


            //Level 4
            for ($i = 34; $i <= 52; $i++) {
                array_push($userEscalations, array(
                    "user" => "ali@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 4,
                ));
            }
            array_push($userEscalations, array(
                "user" => "ali@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => 55, "escalation_level" => 4,
            ));
            for ($i = 56; $i <= 59; $i++) {
                array_push($userEscalations, array(
                    "user" => "ali@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 4,
                ));
            }

            for ($i = 65; $i <= 66; $i++) {
                array_push($userEscalations, array(
                    "user" => "ali@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 4,
                ));
            }
            array_push($userEscalations, array(
                "user" => "ali@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => 70, "escalation_level" => 4,
            ));

            for ($i = 128; $i <= 128; $i++) {
                array_push($userEscalations, array(
                    "user" => "ali@makl.co.ke", "company_id" => 1, "region_id" => $j, "service_issue_id" => $i, "escalation_level" => 4,
                ));
            }


        }


        foreach ($userEscalations as $userEscalation) {
            UserEscalation::updateOrCreate($userEscalation);
        }


    }
}
