<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegionTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::create('facilities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name'); 
            $table->bigInteger('region_id');
            $table->integer('business_filter');
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::create('services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::create('service_issues', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('service_id');
            $table->string('name');
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::create('report_channels', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('user_escalations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user');
            $table->integer('service_issue_id');
            $table->integer('region_id'); 
            $table->integer('escalation_level');
            $table->softDeletes();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('regions');
        Schema::dropIfExists('facilities');
        Schema::dropIfExists('services');
        Schema::dropIfExists('report_channel');
        Schema::dropIfExists('user_escalations');
    }
}
