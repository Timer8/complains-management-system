<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToTableTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->string('client')->nullable();
            $table->bigInteger('facility')->nullable();
            $table->bigInteger('service')->nullable();
            $table->bigInteger('service_issue')->nullable();
            $table->bigInteger('reported_via')->nullable();
            $table->integer('escalation_level')->default(0);
            $table->integer('scheme_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->dropColumn('client');
            $table->dropColumn('facility');
            $table->dropColumn('service');
            $table->dropColumn('service_issue');
            $table->dropColumn('reported_via');
        });
    }
}
