@extends('themes.default1.agent.layout.agent')

@section('sidebar')
    <li class="header">Escalation Matrix</li>
    <li>
        <a href="">
            <i class="fa fa-area-chart"></i> <span>{!! Lang::get('lang.help_topic') !!}</span> <small
                    class="label pull-right bg-green"></small>
        </a>
    </li>
    <li>

    </li>
@stop

@section('Escalations')
    class="active"
@stop

@section('dashboard-bar')
    active
@stop

@section('PageHeader')
    <h1>Escalations</h1>
@stop

@section('dashboard')
    class="active"
@stop

@section('content')
    <!-- check whether success or not -->
    {{-- Success message --}}
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            <i class="fa  fa-check-circle"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{Session::get('success')}}
        </div>
    @endif
    {{-- failure message --}}
    @if(Session::has('fails'))
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <b>{!! Lang::get('lang.alert') !!}!</b>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{Session::get('fails')}}
        </div>
    @endif
    <link type="text/css" href="{{asset("lb-faveo/css/bootstrap-datetimepicker4.7.14.min.css")}}" rel="stylesheet">
    {{-- <script src="{{asset("lb-faveo/dist/js/bootstrap-datetimepicker4.7.14.min.js")}}" type="text/javascript"></script> --}}

    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">New Escalation</h3>
        </div>
        <div class="box-body">
            <div id="foo">
                <input type="hidden" name="duration" value="" id="duration">
                <input type="hidden" name="default" value="false" id="default">
                <div class="form-group">
                    <div class="row">
                        {!! Form::open(['action'=>'Admin\helpdesk\EscalationController@createService','method'=>'post', 'enctype'=>'multipart/form-data']) !!}
                        <div class='col-sm-2 form-group' id="name">
                            {!! Form::label('name', 'Service Name') !!}
                            {!! Form::text('name',null,['class'=>'form-control','id'=>'name'])!!}
                        </div>
                        <div class='col-sm-1'>
                            {!! Form::label('createservice', ' ') !!}<br>
                            <input type="submit" class="btn btn-primary" value="Create Service" id="createservice">
                        </div>
                        {!! Form::close() !!}
                    </div>

                    <div class="row">
                        {!! Form::open(['action'=>'Admin\helpdesk\EscalationController@createServiceIssue','method'=>'post', 'enctype'=>'multipart/form-data']) !!}
                        <div class='col-sm-2'>
                            {!! Form::label('service', 'Service') !!}
                            <select name="service_id" id="service_id" class="form-control">
                                <?php $services = \App\Model\Bliss\Service::all(); ?>
                                @foreach($services as $service)
                                    <option value="{!! $service->id !!}">{!! $service->name !!}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class='col-sm-2 form-group' id="issue_name_div">
                            {!! Form::label('name', 'Service Issue') !!}
                            {!! Form::text('name',null,['class'=>'form-control','id'=>'name'])!!}
                        </div>
                        <div class='col-sm-1'>
                            {!! Form::label('createserviceissue', ' ') !!}<br>
                            <input type="submit" class="btn btn-primary" value="Create Service Issue"
                                   id="createserviceissue">
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="row">
                        {!! Form::open(['action'=>'Admin\helpdesk\EscalationController@setEscalation','method'=>'post', 'enctype'=>'multipart/form-data']) !!}
                        <div class="col-md-2 form-group {{ $errors->has('company') ? 'has-error' : '' }}">
                            {!! Form::label('company', 'Company') !!}<span class="text-red"> *</span>
                            {!! $errors->first('company', '<spam class="help-block">:message</spam>') !!}
                            <?php
                            $companies = \App\Model\Bliss\Company::all();
                            ?>
                            <select name="company" class="form-control" id="company">

                                @foreach($companies as $company)
                                    <option value="{!! $company['id'] !!}">{!! $company['name'] !!}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-2 form-group {{ $errors->has('service') ? 'has-error' : '' }}">
                            {!! Form::label('service', 'Service') !!}<span class="text-red"> *</span>
                            {!! $errors->first('service', '<spam class="help-block">:message</spam>') !!}
                            <?php
                            $services = \App\Model\Bliss\Service::all();
                            ?>
                            <select name="service" class="form-control" id="service">

                                @foreach($services as $service)
                                    <option value="{!! $service['id'] !!}">{!! $service['name'] !!}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-2 form-group {{ $errors->has('service_issue') ? 'has-error' : '' }}">
                            {!! Form::label('service_issue','Service Issue') !!}<span class="text-red"> *</span>
                            <?php
                            $service_issues = \App\Model\Bliss\ServiceIssue::all();
                            ?>
                            <select name="service_issue" class="form-control" id="service_issue">
                            </select>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    $('select[name="service"]').on('change', function () {
                                        $('select[name="service_issue"]').empty();
                                        var issuez = @json($service_issues, JSON_PRETTY_PRINT);
                                        var service = $('select[name="service"]').val();
                                        $('select[name="service_issue"]').append('<option value=0> select service issue</option>');
                                        $.each(issuez, function (key, value) {
                                            if (value.service_id == service)
                                                $('#service_issue').append('<option value=' + value.id + '> ' + value.name + '</option>');
                                        });
                                    });
                                });
                            </script>
                        </div>
                        <div class="col-md-2 form-group {{ $errors->has('region_id') ? 'has-error' : '' }}">
                            {!! Form::label('region_id', 'Region') !!}<span class="text-red"> *</span>
                            {!! $errors->first('region_id', '<spam class="help-block">:message</spam>') !!}
                            <?php
                            $regions = \App\Model\Bliss\Region::all();
                            ?>
                            <select name="region_id" class="form-control" id="region_id">

                                @foreach($regions as $region)
                                    <option value="{!! $region['id'] !!}">{!! $region['name'] !!}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-1 form-group {{ $errors->has('escalation_level') ? 'has-error' : '' }}">
                            {!! Form::label('escalation_level', 'Level') !!}<span class="text-red"> *</span>
                            {!! $errors->first('escalation_level', '<spam class="help-block">:message</spam>') !!}
                            <?php
                            $escalation_levels = array(
                                array('id' => 1, 'name' => 1),
                                array('id' => 2, 'name' => 2),
                                array('id' => 3, 'name' => 3),
                                array('id' => 4, 'name' => 4),
                            );
                            ?>
                            <select name="escalation_level" class="form-control" id="escalation_level">

                                @foreach($escalation_levels as $escalation_level)
                                    <option value="{!! $escalation_level['id'] !!}">{!! $escalation_level['name'] !!}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-1 form-group {{ $errors->has('minutes') ? 'has-error' : '' }}">
                            {!! Form::label('minutes','Minutes') !!}<span class="text-red"> *</span>
                            {!! Form::text('minutes',null,['class' => 'form-control']) !!}
                        </div>
                        <div class="col-md-2 form-group {{ $errors->has('user') ? 'has-error' : '' }}">
                            {!! Form::label('user','Email') !!}<span class="text-red"> *</span>
                            {!! Form::text('user',null,['class' => 'form-control']) !!}
                        </div>
                        <div class='col-sm-1'>
                            {!! Form::label('setescalation', ' ') !!}<br>
                            <input type="submit" class="btn btn-primary" value="Set Escalation"
                                   id="setescalation">
                        </div>
                        {!! Form::close() !!}

                    </div>

                    <div class="row">

                    </div>
                </div>
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->

    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Escalation Matrix</h3>
        </div>
        <div class="box-body" style="overflow-y: hidden;overflow-x: auto;">
            <table class="table table-bordered" style="height:100%; width: 100%; white-space: nowrap;" id="tabular">
                <thead>
                <tr>
                    <th>Service</th>
                    <th>Sevice Issues</th>
                    <th>Region</th>
                    <th>Esc. Level 1</th>
                    <th>Esc. Level 2</th>
                    <th>Esc. Level 3</th>
                    <th>Esc. Level 4</th>
                </tr>
                </thead>
                <tbody>
                @foreach($services as $service)
                    <tr style="height:100%;">
                        <?php
                        $rowcount = 0;
                        ?>
                        <td>{!! $service->name !!}
                        
                            <!-- <a href="javascript:void(0)" class="btn btn-success" id="edit-service" 
                            data-toggle="modal" data-id="{{ $service->id }}">Edit </a> -->
                            <a type="submit" class="btn btn-danger" value="Delete"
                               href="service/delete/{!! $service->id !!}">Delete</a>
                        </td>

                        <td>
                            <table class="table" style="height:100%;">
                                <tbody>
                                @foreach($service->serviceissues as $serviceIssue)
                                    <tr>
                                        <td style="max-lines: 1">{!! $serviceIssue->name !!}</td>
                                        <td>
                                        <!-- <a href="javascript:void(0)" class="btn btn-success" id="edit-service" 
                                        data-toggle="modal1" data-id="{{ $service->id }}">Edit </a> -->
                                        <a type="submit" class="btn btn-danger" value="Delete"
                                               href="serviceissue/delete/{!! $serviceIssue->id !!}">Delete</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </td>
                        <td>
                            <table class="table" style="height:100%;">
                                <tbody>
                                @foreach($service->serviceissues as $serviceIssue)
                                    <tr>
                                    <?php $esca = $serviceIssue->escalations->filter(function ($item) {
                                        return $item->escalation_level == 1;
                                    });
                                    ?>
                                    @foreach($esca as $e)
                                        <?php
                                        $region = "";
                                        if (isset($e)) {
                                            $region = $e->region->name;

                                        }
                                        ?>
                                        <tr>
                                            <td>
                                                {!! $region !!}
                                            </td>
                                        </tr>
                                        @endforeach
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </td>
                        <td>
                            <table class="table" style="height:100%;">
                                <tbody>
                                @foreach($service->serviceissues as $serviceIssue)
                                    <tr>
                                    <?php $esca = $serviceIssue->escalations->filter(function ($item) {
                                        return $item->escalation_level == 1;
                                    });
                                    ?>
                                    @foreach($esca as $e)
                                        <?php
                                        $level_one_user = "";
                                        $level_one_mins = "";
                                        if (isset($e)) {
                                            $level_one_user = $e->user;
                                            $level_one_mins = $e->minutes;
                                        }
                                        ?>
                                        <tr>
                                            <td><span style="background-color: lightgrey !important"
                                                      title="Tickets will be escalated to this person after {!! $level_one_mins !!} minutes"
                                                      class="label label-info">{!! $level_one_mins !!} Mins</span>&nbsp{!! $level_one_user." " !!}
                                            </td>
                                        </tr>
                                        @endforeach
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </td>
                        <td>
                            <table class="table" style="height:100%;">
                                <tbody>
                                @foreach($service->serviceissues as $serviceIssue)
                                    <tr>
                                    <?php $esca = $serviceIssue->escalations->filter(function ($item) {
                                        return $item->escalation_level == 2;
                                    });
                                    ?>
                                    @foreach($esca as $e)
                                        <?php
                                        $level_one_user = "";
                                        $level_one_mins = "";
                                        if (isset($e)) {
                                            $level_one_user = $e->user;
                                            $level_one_mins = $e->minutes;
                                        }
                                        ?>
                                        <tr>
                                            <td><span style="background-color: lightgrey !important"
                                                      title="Tickets will be escalated to this person after {!! $level_one_mins !!} minutes"
                                                      class="label label-info">{!! $level_one_mins !!} Mins</span>&nbsp{!! $level_one_user." " !!}
                                            </td>
                                        </tr>
                                        @endforeach
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </td>
                        <td>
                            <table class="table" style="height:auto;">
                                <tbody>
                                @foreach($service->serviceissues as $serviceIssue)
                                    <tr>
                                    <?php $esca = $serviceIssue->escalations->filter(function ($item) {
                                        return $item->escalation_level == 3;
                                    });
                                    ?>
                                    @foreach($esca as $e)
                                        <?php
                                        $level_one_user = "";
                                        $level_one_mins = "";
                                        if (isset($e)) {
                                            $level_one_user = $e->user;
                                            $level_one_mins = $e->minutes;
                                        }
                                        ?>
                                        <tr>
                                            <td><span style="background-color: lightgrey !important"
                                                      title="Tickets will be escalated to this person after {!! $level_one_mins !!} minutes"
                                                      class="label label-info">{!! $level_one_mins !!} Mins</span>&nbsp{!! $level_one_user." " !!}
                                            </td>
                                        </tr>
                                        @endforeach
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </td>
                        <td>
                            <table class="table" style="height:100%;">
                                <tbody>
                                @foreach($service->serviceissues as $serviceIssue)
                                    <tr>
                                    <?php $esca = $serviceIssue->escalations->filter(function ($item) {
                                        return $item->escalation_level == 4;
                                    });
                                    ?>
                                    @foreach($esca as $e)
                                        <?php
                                        $level_one_user = "";
                                        $level_one_mins = "";
                                        if (isset($e)) {
                                            $level_one_user = $e->user;
                                            $level_one_mins = $e->minutes;
                                        }
                                        ?>
                                        <tr>
                                            <td><span style="background-color: lightgrey !important"
                                                      title="Tickets will be escalated to this person after {!! $level_one_mins !!} minutes"
                                                      class="label label-info">{!! $level_one_mins !!} Mins</span>&nbsp{!! $level_one_user." " !!}
                                            </td>
                                        </tr>
                                        @endforeach
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
<div class="modal fade" id="crud-modal" aria-hidden="true" >
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="customerCrudModal"></h4>
</div>
<div class="modal-body">
<form name="custForm" action="" method="POST">
<input type="hidden" name="id" id="id" >
@csrf
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>Name:</strong>
<input type="text" name="name" id="name" class="form-control" value="{!! $service->name !!}" placeholder="Name" onchange="validate()" >
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 text-center">
<button type="submit" id="btn-save" name="btnsave" class="btn btn-primary" disabled>Submit</button>
<a href="" class="btn btn-danger">Cancel</a>
</div>
</div>
</form>
</div>
</div>
</div>
</div>

    <div id="refresh">
        <script src="{{asset("lb-faveo/plugins/chartjs/Chart.min.js")}}" type="text/javascript"></script>
    </div>
    <script src="{{asset("lb-faveo/plugins/chartjs/Chart.min.js")}}" type="text/javascript"></script>
    <script type="text/javascript">function deleteServiceIssue(id) {
            console.log('hehehe', id)
            $.ajax({
                type: "DELETE",
                url: "serviceissue/delete/" + id,
                beforeSend: function () {

                },
                success: function (response) {
                    location.reload();
                }
            })
            return false;
        }

        var result1a;
        //    var help_topic_global;
        $(document).ready(function () {


            $('#click_day').click(function () {
                $('#click_week').removeClass('btn-primary');
                $('#click_week').addClass('btn-default');
                $('#click_month').removeClass('btn-primary');
                $('#click_month').addClass('btn-default');
                $('#click_day').removeClass('btn-default');
                $('#click_day').addClass('btn-primary');
                $("#duration").val("day");
                document.getElementById("open").checked = false;
                document.getElementById("closed").checked = false;
                document.getElementById("reopened").checked = false;
                $('#foo').submit();
            });
            $('#click_week').click(function () {
                $('#click_day').removeClass('btn-primary');
                $('#click_day').addClass('btn-default');
                $('#click_month').removeClass('btn-primary');
                $('#click_month').addClass('btn-default');
                $('#click_week').removeClass('btn-default');
                $('#click_week').addClass('btn-primary');
                $("#duration").val("week");
                document.getElementById("open").checked = false;
                document.getElementById("closed").checked = false;
                document.getElementById("reopened").checked = false;
                $('#foo').submit();
            });
            $('#click_month').click(function () {
                $('#click_week').removeClass('btn-primary');
                $('#click_week').addClass('btn-default');
                $('#click_day').removeClass('btn-primary');
                $('#click_day').addClass('btn-default');
                $('#click_month').removeClass('btn-default');
                $('#click_month').addClass('btn-primary');
                $("#duration").val("month");
                document.getElementById("open").checked = false;
                document.getElementById("closed").checked = false;
                document.getElementById("reopened").checked = false;
                $('#foo').submit();
            });
            $('#submit').click(function () {
                $('#click_week').removeClass('btn-primary');
                $('#click_week').addClass('btn-default');
                $('#click_day').removeClass('btn-primary');
                $('#click_day').addClass('btn-default');
                $('#click_month').removeClass('btn-primary');
                $('#click_month').addClass('btn-default');
                $("#duration").val('');
            });

            $('#foo').submit(function (event) {
                // get the form data
                // there are many ways to get this data using jQuery (you can use the class or id also)
                var date1 = $('#datepicker4').val();
                var date2 = $('#datetimepicker3').val();
                var help_topic = $('#help_topic').val();
                var duration = $('#duration').val();
                var data = $('#foo').serialize();
                // process the form
                $.ajax({
                    type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
                    url: 'help-topic-report/' + dateData + '/' + formData + '/' + help_topic, // the url where we want to POST
                    dataType: 'json', // what type of data do we expect back from the server
                    data: data, // our data object

                    success: function (result2) {
                        window.result1a = result2;
                        for (var i = 0; i < result2.length; i++) {
                            labels.push(result2[i].date);
                            var date123 = result2[i].date;
                        }

                    }
                });
                // using the done promise callback
                // stop the form from submitting the normal way and refreshing the page
                event.preventDefault();
            });
        });
    </script>
    <script type='text/javascript'>
    $('body').on('click', '#edit-service', function () {
var id = $(this).data('id');
$.get('service/'+id+'/edit', function (data) {
$('#customerCrudModal').html("Edit Service");
$('#btn-update').val("Update");
$('#btn-save').val("edit-service");
 $('#btn-save').prop('disabled',false);
$('#crud-modal').modal('show');
$('#id').val(data.id);
$('#name').val(data.name);
})
});
</script>
    <script>
        $(function () {
//    $("#tabular").DataTable();
            $('#tabular').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
        });
    </script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            // Close a ticket
            $('#close').on('click', function (e) {
                $.ajax({
                    type: "GET",
                    url: "agen",
                    beforeSend: function () {

                    },
                    success: function (response) {

                    }
                })
                return false;
            });
        });
    </script>



    <script src="{{asset("lb-faveo/plugins/moment-develop/moment.js")}}" type="text/javascript"></script>
    <script src="{{asset("lb-faveo/js/bootstrap-datetimepicker4.7.14.min.js")}}" type="text/javascript"></script>
@stop