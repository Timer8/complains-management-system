<!-- @section('search') -->
@extends('themes.default1.client.layout.client') 

@section('title')
    {!! Lang::get('lang.submit_a_ticket') !!} -
@stop

@section('submit')
    class = "active"
@stop
<!-- breadcrumbs -->
 
@section('breadcrumb')
    <div class="site-hero clearfix">
        <ol class="breadcrumb breadcrumb-custom">
            <li class="text">{!! Lang::get('lang.you_are_here') !!}:</li>
            <li><a href="{!! URL::route('form') !!}">{!! Lang::get('lang.submit_a_ticket') !!}</a></li>
        </ol>
    </div>
@stop
<!-- /breadcrumbs -->
{{-- @section('check')
    <div class="banner-wrapper  clearfix">
        <h3 class="banner-title text-center text-info h4">{!! Lang::get('lang.have_a_ticket') !!}?</h3>
        @if(Session::has('check'))
            @if (count($errors) > 0)
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <b>{!! Lang::get('lang.alert') !!} !</b>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </div>
            @endif
        @endif
        <div class="banner-content text-center">
            {!! Form::open(['url' => 'checkmyticket' , 'method' => 'POST'] )!!} 
            {!! Form::label('ticket_number',Lang::get('lang.ticket_number')) !!}<span class="text-red"> *</span>
            {!! Form::text('ticket_number',null,['class' => 'form -control', 'readonly'=>'true']) !!}
            <input type="button" Value="Submit" onclick="this.disabled=true;this.value='Sorry, Not Yet in Operation...';"class="btn btn-info">
            <!-- <input type="submit"  value="{!! Lang::get('lang.check_ticket_status') !!}" class="btn btn-info"> -->
            {!! Form::close() !!}
        </div>
    </div>
@stop --}}
 
  
<!-- content -->
 
@section('content')
    <div id="content" class="site-content col-md-9">
        @yield('extra-content')
        <div class="banner-wrapper  clearfix">
            <h3 class="banner-title text-left text-info h4">{!! Lang::get('lang.search_member_no') !!}</h3>

        <table class="table table-bordered"> 
            <tr>
                <td scope="col-md-12"> 

           <div class="banner-content text-center">

                <form action="{{ route('search-member-form') }}" method="GET">
                <div class="col-md-12 form-group "> 
                <b><input type="text" name="memberno" placeholder="Member No or Mobile No"> </b>
                <span><input type="submit" value="Search" class="btn btn-info"></span>

                </div>
                </form>  
                @if(isset($response['records']))
                <table class="table table-striped" id ='table'>
                <tr>
                <!-- <th>ID</th> -->
                <th>#</th>
                <th>Name</th>
                <th>Member No</th>
                <th>Mobile Number</th>
                <th>Patient Code</th>
                <th>Member Type</th>
                </tr>
                @foreach($response['records'] as $key=>$response)
                <tr>
                    <td align="left"><input type="radio" name="col-1" data-row=""> </td>
                    <td align="left">{{ $response['patientfullname'] }}</td>
                    <td align="left">{{ $response['memberno'] }}</td> 
                    <td align="left">{{ $response['mobilenumber'] }}</td>
                    <td align="left">{{ $response['patientcode'] }}</td>
                    <td align="left">{{ $response['membertype'] }}</td>
                    <td align="left"hidden>{{ $response['locationname'] }}</td>
                    <td align="left"hidden>{{ $response['updated_at'] }}</td>
                    <td align="left" hidden>{{ $response['accountfullname'] }}</td>
                    </tr>
                    @endforeach
                </table>
                    
                @elseif(isset($response))

                <table class="table table-striped" id ='table'>
                <tr>
                <th>#</th>
                <th>Name</th>
                <th>Member No</th>
                <th>Mobile Number</th>
                <th>Patient Code</th>
                <th>Member Type</th>
                </tr>
                <tr>
                <td align="left"><input type="radio" name="col-1" data-row=""> </td>
                <td align="left">{{ $response['patientfullname'] }}</td>
                <td align="left">{{ $response['memberno'] }}</td> 
                <td align="left">{{ $response['mobilenumber'] }}</td>
                <td align="left">{{ $response['patientcode'] }}</td>
                <td align="left">{{ $response['membertype'] }}</td>
                <td align="left"hidden>{{ $response['locationname'] }}</td>
                <td align="left"hidden>{{ $response['updated_at'] }}</td>
                <td align="left" hidden>{{ $response['accountfullname'] }}</td>
                </tr>
                </table>

                @endif 
            </div>
            
        </div>

    </td>
    </tr>
    <table class="table table-striped" id ='table'>
    <tr>
        
        <td scope="col-md-12">
            <button type="button" class="col-md-12 text-left btn-primary btn-xs">Client Results</button>

        @if(Session::has('message'))
            <div class="alert alert-success alert-dismissable">
                <i class="fa  fa-check-circle"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {!! Session::get('message') !!}
            </div>
        @endif
        @if (count($errors) > 0)
            @if(Session::has('check'))
                <?php goto a; ?>
            @endif
            @if(!Session::has('error'))
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <b>{!! Lang::get('lang.alert') !!} !</b>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <?php a: ?>
        @endif
    <!-- open a form -->
        {{-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script> --}}
        <script src="{{asset("lb-faveo/js/jquery2.0.2.min.js")}}" type="text/javascript"></script>
        <!--
        |====================================================
        | SELECT FROM
        |====================================================
        -->
        <?php
        $encrypter = app('Illuminate\Encryption\Encrypter');
        $encrypted_token = $encrypter->encrypt(csrf_token());
        ?>
        <input id="token" type="hidden" value="{{$encrypted_token}}">
        {!! Form::open(['action'=>'Client\helpdesk\FormController@postedForm','method'=>'post', 'enctype'=>'multipart/form-data']) !!}
         
      
    <tr>
      <td scope="col-md-7">  

            @if(Auth::user())

                {!! Form::hidden('Name',Auth::user()->user_name,['class' => 'form-control input-sm']) !!}

            @else
                <div class="col-md-4 form-group {{ $errors->has('Name') ? 'has-error' : '' }}">
                    {!! Form::label('Name',Lang::get('lang.name')) !!}<span class="text-red"> *</span>
                    {!! Form::text('Name',null,['class' => 'form-control input-sm']) !!}
                </div>
            @endif



            @if(Auth::user())

                {!! Form::hidden('Email',Auth::user()->email,['class' => 'form-control input-sm']) !!}

            @else
                <div class="col-md-3 form-group {{ $errors->has('Email') ? 'has-error' : '' }}">
                    {!! Form::label('Email',Lang::get('lang.email')) !!}
                    @if($email_mandatory->status == 1 || $email_mandatory->status == '1')
                        <!-- <span class="text-red"> *</span> -->
                    @endif
                    {!! Form::email('Email',null,['class' => 'form-control input-sm']) !!}
                </div>
                 
            @endif
            <div class="col-md-4 form-group {{ $errors->has('patientfullname') ? 'has-error' : '' }}">
                    {!! Form::label('patientfullname','Client Full Name') !!}
                    {!! Form::text('patientfullname',null,['class' => 'form-control input-sm','placeholder'=>'Client Full Name']) !!} 
                    
                </div>
            <div class="col-md-2 form-group {{ $errors->has('memberno') ? 'has-error' : '' }}">
                    {!! Form::label('memberno','Member No') !!}
                    {!! Form::text('memberno',null,['class' => 'form-control input-sm','placeholder'=>'Member No','readonly'=>'true']) !!}    
                </div>
            <div class="col-md-3 form-group {{ $errors->has('mobile') ? 'has-error' : '' }}">
                    {!! Form::label('mobile','Mobile No') !!}
                    {!! Form::text('mobile',null,['class' => 'form-control input-sm','placeholder'=>'Mobile No']) !!} 
                    
                </div> 
                <div class="col-md-3 form-group "><!-- {{ $errors->has('membertype') ? 'has-error' : '' }} -->
                    {!! Form::label('membertype',Lang::get('lang.member_type')) !!}
                    {!! Form::text('membertype',null,['class' => 'form-control input-sm','placeholder'=>'Principal/Dependent','readonly' => 'true']) !!}
                </div>  
        </td>   
    </tr> 
    <tr>
      <td scope="col-md-12">  
                    <div class="col-md-5 form-group "><!-- {{ $errors->has('locationname') ? 'has-error' : '' }} -->
                        {!! Form::label('locationname','Client Location') !!}
                        {!! Form::text('locationname',null,['class' => 'form-control input-sm','placeholder'=>'Client Location']) !!}
                    </div>
                    <div class="col-md-3 form-group "><!-- {{ $errors->has('updated_at') ? 'has-error' : '' }} -->
                        {!! Form::label('updated_at','Recent Visit Date') !!}
                        {!! Form::text('updated_at',null,['class' => 'form-control input-sm','placeholder'=>'Date','readonly' => 'true']) !!}
                    </div>
                    <div class="col-md-4 form-group "><!-- {{ $errors->has('accountfullname') ? 'has-error' : '' }} -->
                        {!! Form::label('accountfullname','Scheme') !!}
                        {!! Form::text('accountfullname',null,['class' => 'form-control input-sm','placeholder'=>'Scheme','readonly' => 'true']) !!}
                    </div>
                    @if(!Auth::user())

                    <!-- <div class="col-md-2 form-group {{ Session::has('country_code_error') ? 'has-error' : '' }}">
                        {!! Form::label('Code',Lang::get('lang.country-code')) !!}
                        @if($email_mandatory->status == 0 || $email_mandatory->status == '0')
                            <span class="text-red"> *</span>
                        @endif

                        {!! Form::text('Code',null,['class' => 'form-control input-sm', 'placeholder' => $phonecode, 'title' => Lang::get('lang.enter-country-phone-code')]) !!}
                    </div> -->
                    
                    <div class="col-md-2 form-group {{ $errors->has('mobile') ? 'has-error' : '' }}">
                        {!! Form::label('mobile',Lang::get('lang.mobilenumber')) !!}
                        @if($email_mandatory->status == 0 || $email_mandatory->status == '0')
                            <span class="text-red"> *</span>
                        @endif
                        {!! Form::text('mobile',null,['class' => 'form-control input-sm']) !!}
                    </div>
                    <!-- <div class="col-md-5 form-group {{ $errors->has('Phone') ? 'has-error' : '' }}">
                        {!! Form::label('Phone',Lang::get('lang.phone')) !!}
                        {!! Form::text('Phone',null,['class' => 'form-control input-sm']) !!}
                    </div> -->
                    @else
                    {!! Form::hidden('mobile',Auth::user()->mobile,['class' => 'form-control input-sm']) !!}
                    {!! Form::hidden('Code',Auth::user()->country_code,['class' => 'form-control input-sm']) !!}
                    {!! Form::hidden('Phone',Auth::user()->phone_number,['class' => 'form-control input-sm']) !!}

                    @endif
                    <div class="col-md-5 form-group {{ $errors->has('client') ? 'has-error' : '' }}">
                        {!! Form::hidden('client',null,['class' => 'form-control input-sm','placeholder'=>'Ticket No|Service|Service Issue|','readonly'=>'true']) !!} 
                    </div> 
 
        </td>
    </tr>
  </table>
        <div class="content-header">
                <h4>{!! Lang::get('lang.ticket') !!} </h4>
         </div> 
         <div class="row col-md-12">
                <button type="button" class="col-md-12 text-left btn-primary btn-xs">Create New Ticket</button>

                <div class="col-md-3 form-group {{ $errors->has('company') ? 'has-error' : '' }}">
                    {!! Form::label('company','Company') !!}<span class="text-red"> *</span>
                    <?php
                    $companies = \App\Model\Bliss\Company::all();
                    ?>
                    <select name="company" class="form-control" id="selectid">

                        @foreach($companies as $company)
                            <option value="{!! $company['id'] !!}">{!! $company['name'] !!}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-3 form-group {{ $errors->has('client_location') ? 'has-error' : '' }}">
                    {!! Form::label('client_location','Client Locations') !!}<span class="text-red"> *</span>
                    <?php
                    $client_locations = \App\Model\Bliss\Facility::orderBy('name')->get();
                    ?>
                    <select name="client_location" class="form-control" id="client_location">
                    </select>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $('select[name="company"]').on('change', function () {
                                $('select[name="client_location"]').empty();
                                var location = @json($client_locations, JSON_PRETTY_PRINT);
                                var company = $('select[name="company"]').val();
                                $('select[name="client_location"]').append('<option value=0> select service issue</option>');
                                $.each(location, function (key, value) {
                                    if (value.company_id == company)
                                        $('#client_location').append('<option value=' + value.id + '> ' + value.name + '</option>');
                                });
                            });
                        });
                    </script>
                </div>
                <div class="col-md-3 form-group {{ $errors->has('service') ? 'has-error' : '' }}">
                    {!! Form::label('service', 'Service') !!}<span class="text-red"> *</span>
                    {!! $errors->first('service', '<spam class="help-block">:message</spam>') !!}
                    <?php
                    $services = \App\Model\Bliss\Service::all();
                    ?>
                    <select name="service" class="form-control" id="service">

                        @foreach($services as $service)
                            <option value="{!! $service['id'] !!}">{!! $service['name'] !!}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-3 form-group {{ $errors->has('service_issue') ? 'has-error' : '' }}">
                    {!! Form::label('service_issue','Service Issue') !!}<span class="text-red"> *</span>
                    <?php
                    $service_issues = \App\Model\Bliss\ServiceIssue::all();
                    ?>
                    <select name="service_issue" class="form-control" id="service_issue">
                    </select>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $('select[name="service"]').on('change', function () {
                                $('select[name="service_issue"]').empty();
                                var issuez = @json($service_issues, JSON_PRETTY_PRINT);
                                var service = $('select[name="service"]').val();
                                $('select[name="service_issue"]').append('<option value=0> select service issue</option>');
                                $.each(issuez, function (key, value) {
                                    if (value.service_id == service)
                                        $('#service_issue').append('<option value=' + value.id + '> ' + value.name + '</option>');
                                });
                            });
                        });
                    </script>
                </div>
                

                <!-- priority -->
                <?php
                $Priority = App\Model\helpdesk\Settings\CommonSettings::select('status')->where('option_name', '=', 'user_priority')->first();
                $user_Priority = $Priority->status;
                ?>

                @if(Auth::user())

                    @if(Auth::user()->active == 1)
                        @if($user_Priority == 1)


                            <div class="col-md-12 form-group">
                                <div class="row">
                                    <div class="col-md-1">
                                        <label>{!! Lang::get('lang.priority') !!}:</label>
                                    </div>
                                    <div class="col-md-12">
                                        <?php $Priority = App\Model\helpdesk\Ticket\Ticket_Priority::where('status', '=', 1)->get(); ?>
                                        {!! Form::select('priority', ['Priority'=>$Priority->pluck('priority_desc','priority_id')->toArray()],null,['class' => 'form-control input-sm select']) !!}
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endif
                @endif


                <div class="col-md-9 form-group {{ $errors->has('Subject') ? 'has-error' : '' }}">
                    {!! Form::label('Subject',Lang::get('lang.subject')) !!}<span class="text-red"> *</span>
                    {!! Form::text('Subject',null,['class' => 'form-control input-sm','maxlength' => '50']) !!}
                </div>

                <div class="col-md-3 form-group {{ $errors->has('reported_via') ? 'has-error' : '' }}">
                    {!! Form::label('reported_via', 'Reported Via' ) !!}<span class="text-red">*</span>
                    {!! $errors->first('reported_via',null, '<spam class="help-block">:message</spam>') !!}
                    <?php
                    $reportedvia = \App\Model\Bliss\ReportChannel::all();
                    ?>
                    <select name="reported_via" class="form-control" id="selectid">

                        @foreach($reportedvia as $reported)
                            <option value="{!! $reported['id'] !!}">{!! $reported['name'] !!}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-3 form-group {{ $errors->has('scheme_id') ? 'has-error' : '' }}">
                    {!! Form::hidden('scheme_id','Scheme') !!}<span class="text-red"> </span>
                    
                </div>
                <div class="col-md-12 form-group {{ $errors->has('Details') ? 'has-error' : '' }}">
                    {!! Form::label('Details',Lang::get('lang.message')) !!}<span class="text-red"> *</span>
                    {!! Form::textarea('Details',null,['class' => 'form-control input-sm', 'size' => '30x3']) !!}
                </div>
                {{-- <div class="col-md-12 form-group">
                    <div class="btn btn-default btn-file"><i
                                class="fa fa-paperclip"> </i> {!! Lang::get('lang.attachment') !!}<input disabled
                                                                                                         type="file"
                                                                                                         name="attachment[]"
                                                                                                         multiple/>
                    </div> 
                    {!! Lang::get('lang.max') !!}. 10MB
                </div> --}}
                

 
                {{-- Event fire --}}
                <?php Event::fire(new App\Events\ClientTicketForm()); ?>
                <div class="col-md-12" id="response"></div>
                <div id="ss" class="xs-md-6 form-group {{ $errors->has('') ? 'has-error' : '' }}"></div>
                <div class="col-md-12 form-group">{!! Form::submit(Lang::get('lang.Send'),['class'=>'form-group btn btn-info pull-left', 'onclick' => 'this.disabled=true;this.value="Sending, please wait...";this.form.submit();'])!!}</div>
            </div>
            <div class="col-md-12" id="response"></div>
            <div id="ss" class="xs-md-6 form-group {{ $errors->has('') ? 'has-error' : '' }}"></div>
        </div>
        {!! Form::close() !!}
    </div>
    <!--
    |====================================================
    | SELECTED FORM STORED IN SCRIPT
    |====================================================
    -->
    <script type="text/javascript">
        $(document).ready(function () {
            var helpTopic = $("#selectid").val();
            send(helpTopic);
            $("#selectid").on("change", function () {
                helpTopic = $("#selectid").val();
                send(helpTopic);
            });

            function send(helpTopic) {
                $.ajax({
                    url: "{{url('/get-helptopic-form')}}",
                    data: {'helptopic': helpTopic},
                    type: "GET",
                    dataType: "html",
                    success: function (response) {
                        $("#response").html(response);
                    },
                    error: function (response) {
                        $("#response").html(response);
                    }
                });
            }
        });

        $(function () {
//Add text editor
            $("textarea").wysihtml5();
        });
    </script>
   
    

<script>
    
    var table = document.getElementById('table');
    
    for(var i = 1; i < table.rows.length; i++)
    {
        table.rows[i].onclick = function()
        {
             //rIndex = this.rowIndex;
             document.getElementById("patientfullname").value = this.cells[1].innerHTML;
             document.getElementById("memberno").value = this.cells[2].innerHTML;
             document.getElementById("mobile").value = this.cells[3].innerHTML;  
             document.getElementById("membertype").value = this.cells[5].innerHTML;
             document.getElementById("locationname").value = this.cells[6].innerHTML;
             document.getElementById("updated_at").value = this.cells[7].innerHTML;
             document.getElementById("accountfullname").value = this.cells[8].innerHTML;
        };
    }
    var row, el;

        $("input[type=radio]").click(function() {
        el = $(this);
        row = el.data("row");
        $("input[data-row=" + row + "]").prop("checked", false);
        el.prop("checked", true);
        });

</script>
 

@stop
