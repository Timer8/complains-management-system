
<html>
    <head>
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <title>Medbook</title>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    </head>
    <body>
     <div class="container">    
        <br />
        <h3 align="center">Master Report</h3>
        <br />
      <div class="table-responsive">
       <table class="table table-bordered table-striped">
              <thead>
               <tr>
                   <th>Ticket ID</th>
                   <th>Reference No</th>
                   <th>Patient Name</th>
                   <th>Memeber No</th> 
                   <th>Subject</th>
                   <th>Date Logged</th>
                   <th>Logged By</th>
                   <th>Escalated to</th> 
                   <th>Service</th> 
                   <th>Service Issue</th>
                   <th>Re-Assigned</th>
                   <th>Closed</th>
                   <th>Date Closed</th>
                   <th>Location Name</th>
                   <th>Scheme</th>
                   <th>Reported Via</th>  
               </tr>
              </thead>
              <tbody>  
              @foreach($results as $row)
               <tr> 
                <td>{{ $row->Ticket  }}  </td> 
                <td>{{ $row->Reference }}</td> 
                <td>{{ $row->Patient }}</td> 
                <td>{{ $row->Member_No }}</td>
                <td>{{ $row->Subject }}</td>  
                <td>{{ $row->Date_Logged }}</td> 
                <td>{{ $row->Logged_By }}</td> 
                <td>{{ $row->Escalated_to }}</td> 
                <td>{{ $row->Service }}</td> 
                <td>{{ $row->Service_Issue }}</td> 
                <td>{{ $row->Re_Assigned }}</td> 
                <td>{{ $row->Closed }}</td> 
                <td>{{ $row->Date_Closed }}</td> 
                <td>{{ $row->Location_Name }}</td> 
                <td>{{ $row->Scheme }}</td> 
                <td>{{ $row->Reported_Via }}</td> 
               </tr>
              @endforeach
              </tbody>
          </table>
      </div>
     </div>
    </body>
   </html>
   
   