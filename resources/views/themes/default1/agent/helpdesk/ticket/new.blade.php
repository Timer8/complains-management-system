@extends('themes.default1.agent.layout.agent')

@section('Tickets')
    class="active"
@stop

@section('ticket-bar')
    active
@stop

@section('newticket')
    class="active"
@stop

@section('PageHeader')
    <h1>{{Lang::get('lang.tickets')}}</h1>
@stop

@section('content')
    <!-- Main content -->
    {!! Form::open(['route'=>'post.newticket','method'=>'post','id'=>'form']) !!}
    <div class="box box-primary">
        <div class="box-header with-border" id='box-header1'>
            <h3 class="box-title">{!! Lang::get('lang.create_ticket') !!}</h3>
            @if(Session::has('success'))
                <br><br>
                <div class="alert alert-success alert-dismissable">
                    <i class="fa  fa-check-circle"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{Session::get('success')}}
                </div>
            @endif
        <!-- failure message -->
            @if(Session::has('fails'))
                <br><br>
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <b>{!! Lang::get('lang.alert') !!}!</b>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{Session::get('fails')}}
                </div>
            @endif
            @if(Session::has('errors'))
                <br><br>
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <b>{!! Lang::get('lang.alert') !!}!</b>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <br/>
                    @if($errors->first('email'))
                        <li class="error-message-padding">{!! $errors->first('email', ':message') !!}</li>
                    @endif
                    @if($errors->first('first_name'))
                        <li class="error-message-padding">{!! $errors->first('first_name', ':message') !!}</li>
                    @endif
                     @if($errors->first('client'))
                        <li class="error-message-padding">{!! $errors->first('client', ':message') !!}</li>
                    @endif 
                    @if($errors->first('phone'))
                        <li class="error-message-padding">{!! $errors->first('phone', ':message') !!}</li>
                    @endif 
                    @if($errors->first('subject'))
                        <li class="error-message-padding">{!! $errors->first('subject', ':message') !!}</li>
                    @endif
                    @if($errors->first('body'))
                        <li class="error-message-padding">{!! $errors->first('body', ':message') !!}</li>
                    @endif 
                    @if($errors->first('code'))
                        <li class="error-message-padding">{!! $errors->first('code', ':message') !!}</li>
                    @endif 
                    @if($errors->first('mobile'))
                        <li class="error-message-padding">{!! $errors->first('mobile', ':message') !!}</li>
                    @endif
                </div>
            @endif
        </div><!-- /.box-header -->
        
        <tr>
        <div class="banner-wrapper  clearfix">
            <h3 class="banner-title text-left text-info h4">{!! Lang::get('lang.search_member_no') !!}</h3>
            <!-- @if(Session::has('check'))
                @if (count($errors) > 0)
                    <div class="alert alert-danger alert-dismissable">
                        <i class="fa fa-ban"></i>
                        <b>{!! Lang::get('lang.search_alert') !!} !</b>
                        <button type="button" class="close" data-dismiss="search_alert" aria-hidden="true">&times;</button>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </div>
                @endif
            @endif -->
            <div class="banner-content text-center">
                {!! Form::open(['url' => 'searchMember' , 'method' => 'POST'] )!!} 
                <div class="search_member">
                 <div class="col-sm-2 form-group"><!--{{ $errors->has('scheme_id') ? 'has-error' : '' }} -->
                    {!! Form::label('scheme_id_search','Scheme') !!}<span class="text-red"> *</span>
                    <?php
                    $schemes1 = array(
                        
                        array('id1' => 8, "scheme_code" => "KWFT 1", "scheme_name_search" => "KWFT AFYAFIT"),
                        array('id1' => 9, "scheme_code" => "Pan001", "scheme_name_search" => "Panari Hotels Nairobi"),
                        array('id1' => 11, "scheme_code" => "MFI", "scheme_name_search" => "MFI Solutions Limited"),
                        array('id1' => 12, "scheme_code" => "EDU AFYA", "scheme_name_search" => "Ambulance Services"),
                        array('id1' => 13, "scheme_code" => "Welding Alloys", "scheme_name_search" => "Welding Alloys"),
                        array('id1' => 14, "scheme_code" => "Panari Nyahururu", "scheme_name_search" => "Panari Nyahururu"),
                        array('id1' => 19, "scheme_code" => "Bliss", "scheme_name_search" => "Healthier Kenya"),
                        array('id1' => 20, "scheme_code" => "Cap 101", "scheme_name_search" => "Capital Scheme"),
                        array('id1' => 21, "scheme_code" => "CHFP", "scheme_name_search" => "Chandarana FoodPlus"),
                        array('id1' => 22, "scheme_code" => "Nyandarua", "scheme_name_search" => "Nyandarua County"),
                        array('id1' => 25, "scheme_code" => "NCC2020 - 2021", "scheme_name_search" => "NCC 2020-2021"),
                        array('id1' => 26, "scheme_code" => "Busia County 2020-2021", "scheme_name_search" => "Busia County 2020-2021"),
                        array('id1' => 27, "scheme_code" => "Kwale County 2020-2021", "scheme_name_search" => "Kwale County 2020-2021"),
                        array('id1' => 28, "scheme_code" => "KENYA NATIONAL LIBRARY SERVICES", "scheme_name_search" => "KENYA NATIONAL LIBRARY SERVICES MEDICAL  SCHEME 2020-2021"),
                        array('id1' => 29, "scheme_code" => "TSC - 2020 - 2021", "scheme_name_search" => "TSC - 2020 - 2021"),
                    );
                    ?>
                    <select name="scheme_id_search" class="form-control" id="scheme_id_search">

                        @foreach($schemes1 as $scheme_search)
                            <option value="{!! $scheme_search['id1'] !!}">{!! $scheme_search['scheme_name_search'] !!}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-1 form-group "><!-- {{ $errors->has('Member_No') ? 'has-error' : '' }} -->
                {!! Form::label('member_no',Lang::get('lang.member_no')) !!}<span class="text-red"> *</span>
                {!! Form::text('member_no',null,['class' => 'form-control input-sm','placeholder'=>'Member No']) !!}
                </div>
                <br/> <span>
                {!! Form::submit(Lang::get('lang.search_member_no'),
            ['class'=>'form-group btn btn-info pull-left', 'onclick' => 'this.disabled=true;
            this.value="Searching, please wait...";this.form.submit1();']) !!}</span>
            
            <!-- <form id="search-form" class="navbar-form navbar-right" method="get"> 
                <div class="form-group">
                    <input type="text"  class="form-control" id="search" placeholder="Search Here.." 
                    value="{{ request('search') }}">
                </div>
                 <button type="submit" id="search" class="btn btn-default">Submit</button>
            </form> -->

            </div>
        </div>
        </div>
        <div class="box-header with-border">
            <h4 class="box-title">{!! Lang::get('lang.user_details') !!}:</h4>
        </div>
         
        <div class="box-body">

            <div class="form-group">
                <div class="row">
                    

                    <div class="col-sm-1">
                        <!-- email -->
                        <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
                            {!! Form::label('email',Lang::get('lang.first_name')) !!} <span class="text-red"> *</span>
                        <!--  {!! Form::text('email',null,['class' => 'form-control'],['id' => 'email']) !!} -->
                            <input type="text" name="first_name" id="first_name" class="form-control">
                        </div>
                    </div>

                    <div class="col-sm-1">
                        <!-- full name -->
                        <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
                            {!! Form::label('fullname',Lang::get('lang.last_name')) !!} <span class="text-red"></span>
                            <input type="text" name="last_name" id="last_name" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <!-- email -->
                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                            {!! Form::label('email',Lang::get('lang.email')) !!}
                            @if ($email_mandatory->status == 1)
                                <span class="text-red"> *</span>
                            @endif

                            {!! Form::text('email',null,['class' => 'form-control', 'id' => 'email']) !!}
                        </div>
                    </div>
                
                
                    <!-- <div class="col-sm-1 form-group {{ Session::has('country_code_error') ? 'has-error' : '' }}">
                        <div class="form-group {{ $errors->has('code') ? 'has-error' : '' }}">
                            {!! Form::label('code',Lang::get('lang.country-code')) !!}
                            @if ($email_mandatory->status == 0 || $settings->status == 1)
                                <span class="text-red"> *</span>
                            @endif

                            {!! Form::text('code',null,['class' => 'form-control', 'id' => 'country_code', 'placeholder' => $phonecode, 'title' => Lang::get('lang.enter-country-phone-code')]) !!}
                        </div>
                    </div> -->
                    <div class="col-sm-1">
                        <!-- phone -->
                        <div class="form-group {{ $errors->has('mobile') ? 'has-error' : '' }}">
                            <label>{!! Lang::get('lang.mobile_number') !!}:</label>
                            @if ($email_mandatory->status == 0 || $settings->status == 1)
                                <span class="text-red"> *</span>
                            @endif
                            {!! Form::input('number','mobile',null,['class' => 'form-control', 'id' => 'mobile']) !!}
                        </div>
                    </div>
                    <div class="col-sm-1 form-group "><!-- {{ $errors->has('Phone') ? 'has-error' : '' }} -->
                        {!! Form::label('Member_type',Lang::get('lang.member_type')) !!}
                        {!! Form::text('Member_type',null,['class' => 'form-control input-sm','placeholder'=>'Principal/Dependent','readonly' => 'true']) !!}
                    </div>

                    <div class="col-sm-1 form-group "> <!-- {{ $errors->has('client001') ? 'has-error' : '' }} -->
                        {!! Form::label('client001','Recent Visit') !!}
                        {!! Form::text('client001',null,['class' => 'form-control input-sm','placeholder'=>'Date|Centre|','readonly'=>'true']) !!} 
                        <!-- {!! Form::text('client',null,['class' => 'form-control input-sm','placeholder'=>'Ticket No.']) !!} -->
                    </div>
                    <div class="col-sm-2 form-group "><!-- {{ $errors->has('client002') ? 'has-error' : '' }} -->
                        {!! Form::label('client002','Open Tickets') !!}
                        {!! Form::text('client002',null,['class' => 'form-control input-sm','placeholder'=>'Ticket No|Service|Service Issue|','readonly'=>'true']) !!} 
                        <!-- {!! Form::text('client',null,['class' => 'form-control input-sm','placeholder'=>'Ticket No.']) !!} --> 
                    </div>
                    <!-- <div class="col-sm-2 form-group ">
                        <button type="button" class="col-sm-2 text-right btn-primary btn-xs">Open <sup><span class="badge badge-light">0</span></sup></button>
                        {!! Form::text('client',null,['class' => 'form-control input-sm','placeholder'=>'Ticket No|Service|Service Issue|','readonly'=>'true']) !!}
                    </div>  -->
                    <!-- <div class="col-sm-5">  -->
                        <!-- phone -->
                    <!--    <div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
                            <label>{!! Lang::get('lang.phone') !!}:</label>
                            {!! Form::input('number','phone',null,['class' => 'form-control', 'id' => 'phone_number']) !!}
                            {!! $errors->first('phone', '<spam class="help-block text-red">:message</spam>') !!}
                        </div>
                    </div> -->
                    <!--  <div class="form-group">
                         <div class="col-sm-2">
                             <label>Ticket Notice:</label>
                         </div>
                         <div class="col-sm-6">
                             <input type="checkbox" name="notice" id=""> Send alert to User
                         </div>
                     </div> -->
                </div>
            </div>
        </div>
        <div class="box-header with-border">
            <h4 class="box-title">{!! Lang::get('lang.ticket_detail') !!}:</h4>
        </div>
        <div class="box-body">
            <!-- ticket details -->
            <div class="col-sm-1 form-group {{ $errors->has('client') ? 'has-error' : '' }}">
                {!! Form::label('client','Client') !!}<span class="text-red"> *</span>
                {!! Form::text('client',null,['class' => 'form-control']) !!}
            </div>

            <div class="col-sm-1 form-group {{ $errors->has('client_location') ? 'has-error' : '' }}">
                    {!! Form::label('company','Company') !!}<span class="text-red"> *</span>
                    <?php
                    $companies = \App\Model\Bliss\Company::all();
                    ?>
                    <select name="company" class="form-control" id="selectid">

                        @foreach($companies as $company)
                            <option value="{!! $company['id'] !!}">{!! $company['name'] !!}</option>
                        @endforeach
                    </select>
            </div>

            <div class="col-sm-2 form-group {{ $errors->has('client_location') ? 'has-error' : '' }}">
                    {!! Form::label('client_location','Client Locations') !!}<span class="text-red"> *</span>
                    <?php
                    $client_locations = \App\Model\Bliss\Facility::all();
                    ?>
                    <select name="client_location" class="form-control" id="client_location">
                    </select>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $('select[name="company"]').on('change', function () {
                                $('select[name="client_location"]').empty();
                                var location = @json($client_locations, JSON_PRETTY_PRINT);
                                var company = $('select[name="company"]').val();
                                $('select[name="client_location"]').append('<option value=0> select service issue</option>');
                                $.each(location, function (key, value) {
                                    if (value.company_id == company)
                                        $('#client_location').append('<option value=' + value.id + '> ' + value.name + '</option>');
                                });
                            });
                        });
                    </script>
                </div>
            <div class="col-sm-1 form-group {{ $errors->has('service') ? 'has-error' : '' }}">
                {!! Form::label('service', 'Service') !!}
                {!! $errors->first('service', '<spam class="help-block">:message</spam>') !!}
                <?php

                $services = \App\Model\Bliss\Service::all();
                ?>
                <select name="service" class="form-control" id="selectid">

                    @foreach($services as $service)
                        <option value="{!! $service['id'] !!}">{!! $service['name'] !!}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-sm-2 form-group {{ $errors->has('service_issue') ? 'has-error' : '' }}">
                {!! Form::label('service_issue','Service Issue') !!}<span class="text-red"> *</span>
                {{--                {!! Form::text('service_issue',null,['class' => 'form-control']) !!}--}}
                <?php
                $service_issues = \App\Model\Bliss\ServiceIssue::all();
                ?>
                <select name="service_issue" class="form-control" id="service_issue">
                </select>
                <script type="text/javascript">
                    $(document).ready(function () {
                        $('select[name="service"]').on('change', function () {
                            $('select[name="service_issue"]').empty();
                            var issuez = @json($service_issues, JSON_PRETTY_PRINT);
                            var service = $('select[name="service"]').val();
                            $('select[name="service_issue"]').append('<option value=0> select service issue</option>');
                            $.each(issuez, function (key, value) {
                                if (value.service_id == service)
                                    $('#service_issue').append('<option value=' + value.id + '> ' + value.name + '</option>');
                            });
                        });
                    });
                </script>
            </div>

            

            <!-- subject -->
            <div class="col-sm-5 form-group {{ $errors->has('subject') ? 'has-error' : '' }}">
                {!! Form::label('subect','Subject') !!}<span class="text-red"> *</span></label>
                {!! Form::text('subject',null,['class' => 'form-control']) !!}

            </div>
            <div class="form-group col-sm-12  {{ $errors->has('body') ? 'has-error' : '' }}">
                <!-- details -->
                        <label>{!! Lang::get('lang.detail') !!}:<span class="text-red"> *</span></label>

                        {!! Form::textarea('body',null,['class' => 'form-control','id' => 'body', 'style'=>"width:100%; height:70px;"]) !!}

            </div>
            <div class="col-sm-3 form-group {{ $errors->has('reported_via') ? 'has-error' : '' }}">
                {!! Form::label('reported_via', 'Reported Via') !!}
                {!! $errors->first('reported_via', '<spam class="help-block">:message</spam>') !!}
                <?php
                $reportedvia = \App\Model\Bliss\ReportChannel::all();
                ?>
                <select name="reported_via" class="form-control" id="selectid">

                    @foreach($reportedvia as $reported)
                        <option value="{!! $reported['id'] !!}">{!! $reported['name'] !!}</option>
                    @endforeach
                </select>
            </div>



        </div>
        <div class="box-footer">
            <div class="row">
                <div class="col-sm-12">
                </div>
                <div class="col-sm-3 form-group">
                    <input type="submit" value="{!! Lang::get('lang.create_ticket') !!}" class="btn btn-primary"
                           onclick="this.disabled=true;this.value='Sending, please wait...';this.form.submit();">
                </div>
            </div>
        </div>
    </div><!-- /. box -->
    {!! Form::close() !!}
    <script type="text/javascript">
        $(document).ready(function () {
            var helpTopic = $("#selectid").val();
            send(helpTopic);
            $("#selectid").on("change", function () {
                helpTopic = $("#selectid").val();
                send(helpTopic);
            });

            function send(helpTopic) {
                $.ajax({
                    url: "{{url('/get-helptopic-form')}}",
                    data: {'helptopic': helpTopic},
                    type: "GET",
                    dataType: "html",
                    success: function (response) {
                        $("#response").html(response);
                    },
                    error: function (response) {
                        $("#response").html(response);
                    }
                });
            }
        });
        $(function () {
            $("textarea").wysihtml5();
        });

        $(document).ready(function () {
            $("#service").change(function () {
                console.log("Handler for .change() called.");
            });
            $('#form').submit(function () {
                var duedate = document.getElementById('datemask').value;
                if (duedate) {
                    var pattern = /^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/;
                    if (pattern.test(duedate) === true) {
                        $('#duedate').removeClass("has-error");
                        $('#clear-up').remove();
                    } else {
                        $('#duedate').addClass("has-error");
                        $('#clear-up').remove();
                        $('#box-header1').append("<div id='clear-up'><br><br><div class='alert alert-danger alert-dismissable'><i class='fa fa-ban'></i><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> Invalid Due date</div></div>");
                        return false;
                    }
                }
            });
        });
        $(document).ready(function () {
            $("#email").autocomplete({
                source: "{!!URL::route('post.newticket.autofill')!!}",
                minLength: 1,
                select: function (evt, ui) {
                    // this.form.phone_number.value = ui.item.phone_number;
                    // this.form.user_name.value = ui.item.user_name;
                    if (ui.item.first_name) {
                        this.form.first_name.value = ui.item.first_name;
                    }
                    if (ui.item.last_name) {
                        this.form.last_name.value = ui.item.last_name;
                    }
                    // if (ui.item.country_code) {
                    //     this.form.country_code.value = ui.item.country_code;
                    // }
                    // if (ui.item.phone_number) {
                    //     this.form.phone_number.value = ui.item.phone_number;
                    // }
                    if (ui.item.mobile) {
                        this.form.mobile.value = ui.item.mobile;
                    }
                }
            });
        });

        $(function () {
            $('#datemask').datepicker({changeMonth: true, changeYear: true}).mask('99/99/9999');
        });
    </script>

@stop




